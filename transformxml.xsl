<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fits="http://hul.harvard.edu/ois/xml/ns/fits/fits_output">
	<xsl:output method="xml" indent="yes" encoding="utf-8"/>

<xsl:template match="node()|@*">
	<xsl:copy>
		<xsl:apply-templates select="node()|@*"/>
	</xsl:copy>
</xsl:template>

<xsl:template match="fits:statistics | fits:tool"/>
<xsl:template match="@version | @timestamp"/>
<xsl:template match="@toolname | @toolversion | @status"/>
<xsl:template match="@format | @mimetype">
	<xsl:element name="{name()}" namespace="http://hul.harvard.edu/ois/xml/ns/fits/fits_output" >
		<xsl:value-of select="."/>
	</xsl:element>
</xsl:template>
</xsl:stylesheet>