<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datamart extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('search_m');
        $this->load->library('pagination');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {
        
        $q = $this->input->get('q');
        $q2 = $this->input->get('q2');
        
        $data =['q' => $q,
                'q2' => $q2];

        // $results1 = $this->search_m->get_all_file();
     
        //juml data
        if($this->input->get('q') || $this->input->get('btncari'))
        {
            $results = $this->search_m->search_bigdata(0,$this->uri->segment(3)); 
            $jml_data = (sizeof($results));
        }else{
            $jml_data=$this->search_m->count_data();
        }
        
        $data['datacount'] = (int)$jml_data;
        $config = array();
        $config["base_url"] = site_url('datamart/index');
        $config["total_rows"] = $jml_data;
        $config["per_page"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';  
        $config['num_tag_close'] = '</li>';        
        $config['uri_segment'] = 3;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        $data['str_links'] = $this->pagination->create_links();
        // $data['links'] = explode('&nbsp;',$str_links );
        $results = $this->search_m->get_all_bigdata($config["per_page"],$this->uri->segment(3));
        if($this->input->get('q') || $this->input->get('btncari'))
        {
              $results = $this->search_m->search_bigdata($config["per_page"],$this->uri->segment(3)); 
        }
        $data['results'] = array();

        $this->session->set_userdata('search',[
            'qfield1'   => $this->input->get('qfield1'),
            'volume' => [
            'size'=> $this->input->get('size1',[
                'Vol1'=> $this->input->get('Vol1'),
                'Vol2'=> $this->input->get('Vol2'),
                'Vol3'=> $this->input->get('Vol3'),
                'Vol4'=> $this->input->get('Vol4'),
                'Vol5'=> $this->input->get('Vol5'),
                ]),
            ],
            'variety' => [
                'img'=> $this->input->get('imgfield1',[
                     'img1'=> $this->input->get('Image1'),
                     'img2'=> $this->input->get('Image2'),
                     'img3'=> $this->input->get('Image3'),
                     'img4'=> $this->input->get('Image4'),
                     'img5'=> $this->input->get('Image5'),
                     'img6'=> $this->input->get('Image6'),
                     'img7' => $this->input->get('Image7'),
                    ]),
                 'doc'=> $this->input->get('docfield1',[
                     'doc1'=> $this->input->get('Doc1'),
                     'doc2'=> $this->input->get('Doc2'),
                     'doc3'=> $this->input->get('Doc3'),
                     'doc4'=> $this->input->get('Doc4'),
                     'doc5'=> $this->input->get('Doc5'),
                     'doc6'=> $this->input->get('Doc6'),
                     'doc7'=> $this->input->get('Doc7'),
                     'doc8'=> $this->input->get('Doc8'),
                     'doc9'=> $this->input->get('Doc9'),
                    ]),
                 'aud'=> $this->input->get('audfield1',[
                     'aud1'=> $this->input->get('Aud1'),
                     'aud2'=> $this->input->get('Aud2'),
                     'aud3'=> $this->input->get('Aud3'),
                     'aud4'=> $this->input->get('Aud4'),
                     'aud5'=> $this->input->get('Aud5'),
                     'aud6'=> $this->input->get('Aud6'),
                     'aud7'=> $this->input->get('Aud7'),
                     'aud8'=> $this->input->get('Aud8'),
                     'aud9'=> $this->input->get('Aud9'),
                     'aud10'=> $this->input->get('Aud10'),
                    ]),
                 'vid'=> $this->input->get('vidfield1',[
                     'vid1'=> $this->input->get('Vid1'),
                     'vid2'=> $this->input->get('Vid2'),
                     'vid3'=> $this->input->get('Vid3'),
                     'vid4'=> $this->input->get('Vid4'),
                     'vid5'=> $this->input->get('Vid5'),
                     'vid6'=> $this->input->get('Vid6'),
                     'vid7'=> $this->input->get('Vid7'),
                    ]),

            ],
            'velocity' => [
                'fitstime'=> $this->input->get('fitstime'),
            ],
            'veracity' => [
                'author'=> $this->input->get('author'),
            ],
             'value' => [
                'kategory'=> $this->input->get('catfield1',[
                    'cat1' => $this->input->get('Cat1'),
                    'cat2' => $this->input->get('Cat2'),
                    'cat3' => $this->input->get('Cat3'),
                    'cat4' => $this->input->get('Cat4'),
                    'cat5' => $this->input->get('Cat5'),
                    'cat6' => $this->input->get('Cat6'),
                    'cat7' => $this->input->get('Cat7'),
                    'cat8' => $this->input->get('Cat8'),
                    'cat9' => $this->input->get('Cat9'),
                    'cat10' => $this->input->get('Cat10'),
                    'cat11' => $this->input->get('Cat11'),
                    'cat12' => $this->input->get('Cat12'),
                    'cat13' => $this->input->get('Cat13'),
                    'cat14' => $this->input->get('Cat14'),
                    'cat15' => $this->input->get('Cat15'),
                    ]),
            ],
        ]);



        if(count($results)>0)
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                'id' => $result['_id']->__toString(),
                'idmeta' => $result['idmeta'],
                'name'=>$result['big_data']['value']['tittle'],
                'title' => $result['big_data']['value']['tittle'],
                'filename' => $result['big_data']['value']['filename'],
                'creator' => $result['big_data']['value']['creator'],
                'format' => $result['big_data']['variety']['Mimetype'],
                'time' => $result['big_data']['time']['lastmodified'],
                'velocity' => $result['big_data']['velocity']['fitsExecutionTime'],
                'veracity' =>  (!empty($result['big_data']['veracity']['well-formed'])) ?
                     ($result['big_data']['veracity']['well-formed']) : null,
                'volume' => $result['big_data']['volume']['size'],
                'category' => $result['big_data']['value']['category'],
                'upload_at'=>$result['upload_at'],
                'description' => $result['big_data']['value']['description']

                ]; 

            }

        }
         $data['authors'] = $this->search_m->get_creator();
         $data['creators']  = array();
         foreach ($data['authors'] as $key => $value) {
             $data['creators'][] = (isset($value['big_data']['value']['creator']) ? $value['big_data']['value']['creator'] : '');
         }
         $data['creators']  = array_unique($data['creators']);
       
        // usort($data['results'], function($a, $b) {
        //     return strtotime(date('Y/m/d h:i:s', $b['upload_at']->sec)) - strtotime(date('Y/m/d h:i:s', $a['upload_at']->sec));
        // });
      
        //jml image
        $jml_img=$this->search_m->count_image();
             $data['imagecount'] = (int)$jml_img;
        //jml dokumen
        $jml_doku=$this->search_m->count_dokumen();
            $data['dokumencount'] = (string)$jml_doku;
        //jml audio
        $jml_audio=$this->search_m->count_audio();
            $data['audiocount']=(string)$jml_audio;
        //jml video
        $jml_video=$this->search_m->count_video();
            $data['videocount']=(string)$jml_video;

        // $data['image']=$this->search_m->get_image();
        // var_dump($data['image']);exit;

        $data['search'] = $this->session->userdata('search');
        $data['title'] = 'Your File';
        $data['page'] = 'datam_v';
        $this->load->view('templates/container',$data);
    }

    function clear(){
       $this->session->unset_userdata('search');
       redirect('datamart','refresh');
    }

    function delete_file($id)
    {
       
        $this->search_m->delete_by_id($id);
        $this->search_m->delete_bigdata_by_id($id);
        redirect('datamart','refresh');

    }

    function update_file()
    {
        $id = $this->uri->segment(3);
        $data['details']= $this->search_m->get_file_by_id($id);
        $mapping=$this->search_m->get_bigdata_by_id($id);
        // echo "<pre>";var_dump($mapping); exit;
        if(count($data['details']) == 0)
        {
            show_404();
        }
        foreach ($data['details'] as $detail) 
        {
            $data['detail'] = [
                'title' => $detail["title"]. ' '. ' Characteristics',
                'identification' => $detail["identification"],
                'fileinfo' => $detail["fileinfo"],
                'filestatus' => $detail['filestatus'],
                'statistics' => $detail['statistics'],
                'metadata' => $detail["metadata"],
            ];
        }

       
    
            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                    ];
    
          // var_dump($data['detil']['value']);exit;
        if($this->input->post('btnSimpan')){
            
            $data = array();
            $data['title'] = $this->input->post('title');
            $data['creator'] = $this->input->post('creator');
            $data['category'] = $this->input->post('category');
            $data['description'] = $this->input->post('description');

            $this->search_m->update_data($id, $data);
            $this->search_m->update_bigdata($id, $data); 
            // echo "<pre>";var_dump($data);exit;
             // var_dump($this->search_m->update_bigdata($id, $data)); exit();
            redirect('datamart/search_result/'.$id);
        }
        
        $data['title'] = 'Edit File';
        $data['page'] = 'edit_v';
        $this->load->view('templates/container',$data);
        
    }

    function search_result($id)
    {
        
        $id = $this->uri->segment(3);
        // $filename = $this->uri->segment(4);
        $data['id'] = $id;
        // var_dump($id);exit();

        $details = $this->search_m->get_file_by_id($id);
        $mapping = $this->search_m->get_bigdata_by_id($id);
        
        // $data['data_xml'] = "uploads/extracted_data/".$filename;

        // echo'<pre>';var_dump($details);exit();
        // echo "aaaaa";
        // exit();

       $image = array('image/jpeg', 'image/png', 'image/gif','image/jpg','image/tiff', 'image/bmp','application/octet-stream','Application/vnd.oasis.opendocument.text','image/vnd.adobe.photoshop');
        $video = array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska');
        $audio = array('audio/mpeg', 'audio/wav','audio/mp3','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts');
        $viewerjs = array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'application/vnd.ms-excel',
                     'video/webm','application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {

            $data['detail'] = [
            '_id' => $detail["_id"],
            'title' => $detail["title"],
            'creator' => 'Creator : '.$detail["creator"],
            'category' => 'Category : '.$detail["category"],
            'description' => $detail["description"],
            'identification' => $detail["identification"],
            'fileinfo' => $detail["fileinfo"],
            'filestatus' => $detail["filestatus"],
            'statistics' => $detail["statistics"],
            'metadata' => $detail["metadata"],
            'comments' => $detail["comments"]
            ];

            if (in_array($detail['identification']['Mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $viewerjs))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/else';
                $data['details'] = $details;               
            }
        }    

            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                        'time'=> $mapping['big_data']['time']
                    ];

                if (in_array($mapping['big_data']['variety']['Mimetype'], $image))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/image';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $video))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/video';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $audio))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/audio';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $viewerjs))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/viewerjs';
                    $data['details'] = array($mapping);
                }
                else
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/else';
                    $data['details'] = array($mapping);               
                }

                // echo'<pre>';var_dump($data);exit();
            
            $this->load->view('templates/container',$data);
        
      }  
}
