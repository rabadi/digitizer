<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xml extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('search_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {

        $q = $this->input->get('q');
        $q2 = $this->input->get('q2');
        
        $data =['q' => $q,
                'q2' => $q2];


        $results1 = $this->search_m->get_all_file();
        $results = $this->search_m->get_all_bigdata();
        // echo "<pre>";var_dump($data);exit();
        $data['results'] = array();

        if($this->input->get('q') || $this->input->get('btncari')){
            
              $results = $this->search_m->search_bigdata(); 

        } 

        if(count($results)>0)
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                'id' => $result['_id']->__toString(),
                'idmeta' => $result['idmeta'],
                'name'=>$result['big_data']['value']['title'],
                'title' => $result['big_data']['value']['title'],
                'filename' => $result['big_data']['value']['filename'],
                'creator' => $result['big_data']['value']['creator'],
                'author' => $result['big_data']['value']['author'],
                'format' => $result['big_data']['variety']['Mimetype'],
                'time' => $result['big_data']['time']['lastmodified'],
                'velocity' => $result['big_data']['velocity']['fitsExecutionTime'],
                'volume' => $result['big_data']['volume']['size'],
                'category' => $result['big_data']['value']['kategori'],
                'description' => $result['big_data']['value']['description']

                // if(!empty($result['big_data']['velocity']['fitsExecutionTime'])){
                // 'velocity' => $result['big_data']['velocity']['fitsExecutionTime'];}
                
                ];
            }
        }

        // echo "<pre>";
        // var_dump($results);
        // exit;



        // usort($data['results'], function($a, $b) {
        //     return strtotime(date('m/d/Y h:i:s', $b['uploaded_at']->sec)) - strtotime(date('m/d/Y h:i:s', $a['uploaded_at']->sec));
        // });

        $data['title'] = 'Your File';
        $data['page'] = 'xml_v';
        $this->load->view('templates/container',$data);
    }


    function result($id)
    {
        $id = $this->uri->segment(3);
        $details = $this->search_m->get_file_by_id($id);

        $image = array('image/jpeg', 'image/png', 'image/gif');
        $video = array('video/mp4');
        $audio = array('audio/mpeg', 'audio/wav');
        $viewerjs = array('application/pdf');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {
            $data['detail'] = [
                '_id' => $detail["_id"],
                'title' => $detail["title"]. ' '. 'Characteristics',
                'creator' => 'Creator : '.$detail["creator"],
                'category' => 'Category : '.$detail["category"],
                'description' => $detail["description"],
                'identification' => $detail["identification"],
                'fileinfo' => $detail["fileinfo"],
                'filestatus' => $detail["filestatus"],
                'statistics' => $detail["statistics"],
                'metadata' => $detail["metadata"],
                'comments' => $detail["comments"],


            ];

            if (in_array($detail['identification']['mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }

            $this->load->view('templates/container',$data);
        }
    }

    function result_bigdata($id)
    {
        
        $id = $this->uri->segment(3);
        // $filename = $this->uri->segment(4);
        $data['id'] = $id;
        // var_dump($id);exit();

        $details = $this->search_m->get_file_by_id($id);
        $mapping = $this->search_m->get_bigdata_by_id($id);
        
        // $data['data_xml'] = "uploads/extracted_data/".$filename;

        // echo'<pre>';var_dump($details);exit();
        // echo "aaaaa";
        // exit();

        $image = array('image/jpeg', 'image/png', 'image/gif');
        $video = array('video/mp4');
        $audio = array('audio/mpeg', 'audio/wav');
        $viewerjs = array('application/pdf', 'application/vnd.oasis.opendocument.presentation');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {

            $data['detail'] = [
            '_id' => $detail["_id"],
            'title' => $detail["title"],
            'creator' => 'Creator : '.$detail["creator"],
            'category' => 'Category : '.$detail["category"],
            'description' => $detail["description"],
            'identification' => $detail["identification"],
            'fileinfo' => $detail["fileinfo"],
            'filestatus' => $detail["filestatus"],
            'statistics' => $detail["statistics"],
            'metadata' => $detail["metadata"],
            'comments' => $detail["comments"]
            ];

            if (in_array($detail['identification']['Mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $viewerjs))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/else';
                $data['details'] = $details;               
            }
        }    

            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                        // 'time' => $mapping['big_data']['time']
                    ];

                // var_dump($mapping['big_data']['value']);exit;
                if (in_array($mapping['big_data']['variety']['Mimetype'], $image))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/image';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $video))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/video';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $audio))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/audio';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $viewerjs))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/viewerjs';
                    $data['details'] = array($mapping);
                }
                else
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/else';
                    $data['details'] = array($mapping);               
                }

            $this->load->view('templates/container',$data);
        
    }             

    function delete_file($id)
    {
        $this->search_m->delete_by_id($id);
        $this->search_m->delete_bigdata_by_id($id);
        redirect('file','refresh');

    }

    function update_file($id, $filename)
    {
        $id = $this->uri->segment(3);
        $filename = $this->uri->segment(4);

        // var_dump($id);exit();
        
        $data['data_xml'] = "uploads/extracted_data/".$filename;

        $data['details']= $this->search_m->get_file_by_id($id);
        $mapping = $this->search_m->get_bigdata_by_id($id);

        // var_dump($data['data_xml']);exit();
        

        // echo "<pre>";var_dump($mapping);exit();
        
        if(count($data['details']) == 0)
        {
            show_404();
        }

        foreach ($data['details'] as $detail) 
        {
            $data['detail'] = [
            'title' => $detail["title"]. ' '. ' Characteristics',
            'identification' => $detail["identification"],
            'fileinfo' => $detail["fileinfo"],
            'filestatus' => $detail['filestatus'],
            'statistics' => $detail['statistics'],
            'metadata' => $detail["metadata"],
            ];
        }



        $data['detil'] = [
        'variety' => $mapping['big_data']['variety'],
        'volume' => $mapping['big_data']['volume'],
        'veracity' => $mapping['big_data']['veracity'],
        'velocity' => $mapping['big_data']['velocity'],
        'value' => $mapping['big_data']['value'],
                        // 'time' => $mapping['big_data']['time']
        ];


        if($this->input->post('btnSimpan'))
        { 
            $data1 = array();
            $data1['title'] = $this->input->post('title');
            $data1['creator'] = $this->input->post('creator');
            $data1['category'] = $this->input->post('category');
            $data1['description'] = $this->input->post('description');

            $this->search_m->update_data($id, $data1);
            $this->search_m->update_bigdata($id, $data1);

            redirect('file/detail/'.$id.'/'.$filename);

        }


        $data['title'] = 'Edit File';
        $data['page'] = 'edit_v';
        $data['id'] = $id;
        $this->load->view('templates/container',$data);
        
    }


    function insert_comment()    
    {
        $id = $this->uri->segment(3); 

        if($this->input->post('btnComment')){
            $comment = array(
                'user_id' => $this->session->userdata('user_id'),
                'fullname' => $this->session->userdata('fullname'),
                'des_comment' => $this->input->post('des_comment'),
                'date' => new MongoDate()
                );

            $this->search_m->insert_comment($id,$comment); 
            redirect('file/detail/'.$id);
        }

    }

    function get_xml($id)
    {
        
        $id = $this->uri->segment(3);
        // var_dump($id);exit();

        $file = $this->search_m->get_file_by_id($id);
        

        $file = $file[0];


        // echo"<pre>";var_dump($file);exit();
        
        // Load XML writer library
        $this->load->library('MY_Xml_Writer');

        // Initiate Class
        $xml = new MY_Xml_Writer;
        $xml->setRootName('Metadata');
        $xml->Initiate();

        // Start branch 1
        $xml->startBranch('identification');

                
                if(!empty($file["identification"]["Mimetype"])){
                $xml->addNode('jenis_file', $file["identification"]['Mimetype']);}
                if(!empty($file["identification"]["Format"])){
                $xml->addNode('format', $file["identification"]['Format']);}
                if(!empty($file["identification"]["version"])){
                $xml->addNode('version', $file["identification"]['version']);}
                if(!empty($file["identification"]["puid"])){
                $xml->addNode('puid', $file["identification"]['puid']);}
                

        // End branch 1
        $xml->endBranch();

        $xml->startBranch('file_info');

                if(!empty($file["fileinfo"]["size"])){
                $xml->addNode('size', $file["fileinfo"]['size']);}
                if(!empty($file["fileinfo"]["lastmodified"])){
                $xml->addNode('last_modified', $file["fileinfo"]['lastmodified']);}
                if(!empty($file["fileinfo"]["filepath"])){
                $xml->addNode('filepath', $file["fileinfo"]['filepath']);}
                if(!empty($file["fileinfo"]["md5checksum"])){
                $xml->addNode('md5checksum', $file["fileinfo"]["md5checksum"]);}
                if(!empty($file["fileinfo"]["filename"])){
                $xml->addNode('nama_file', $file["fileinfo"]['filename']);}
                if(!empty($file["fileinfo"]["creatingApplicationName"])){
                $xml->addNode('creating_application_name', $file["fileinfo"]['creatingApplicationName']);}
                if(!empty($file["fileinfo"]["created"])){
                $xml->addNode('created', $file["fileinfo"]['created']);}
                if(!empty($file["fileinfo"][" fslastmodified"])){
                $xml->addNode(' fslastmodified', $file["fileinfo"][' fslastmodified']);}
               
                
               
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('file_status');

                if(!empty($file["filestatus"]["well_formed"])){
                $xml->addNode('well_formed', $file["filestatus"]['well_formed']);}
                if(!empty($file["filestatus"]["message"])){
                $xml->addNode('message', $file["filestatus"]['message']);}
                if(!empty($file["filestatus"]["valid"])){
                $xml->addNode('valid', $file["filestatus"]['valid']);}
                
                
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('statistics');

                if(!empty($file["statistics"]["fitsExecutionTime"])){
                $xml->addNode('Execution_Time', $file["statistics"]['fitsExecutionTime']);}
                
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('metadata');

        $type="";
        if(!empty($file["metadata"])){
            foreach($file["metadata"] as $d=>$v){
                $type=$d;
            }
        }

                if(!empty($file["metadata"][$type]["byteOrder"])){
                $xml->addNode('byteorder', $file["metadata"][$type]['byteOrder'] );}
                if(!empty($file["metadata"][$type]["title"])){
                $xml->addNode('title', $file["metadata"][$type]['title']);}
                if(!empty($file["metadata"][$type]["pageCount"])){
                $xml->addNode('page_count', $file["metadata"][$type]['pageCount']);}
                if(!empty($file["metadata"][$type]["language"])){
                $xml->addNode('language', $file["metadata"][$type]['language']);}
                if(!empty($file["metadata"][$type]["wordcount"])){
                $xml->addNode('word_count', $file["metadata"][$type]['wordcount']);}
                if(!empty($file["metadata"][$type]["characterCount"])){
                $xml->addNode('character_count', $file["metadata"][$type]['characterCount']);}
                if(!empty($file["metadata"][$type]["graphicsCount"])){
                $xml->addNode('graphic_count', $file["metadata"][$type]['graphicsCount']);}
                if(!empty($file["metadata"][$type]["tableCount"])){
                $xml->addNode('table_count', $file["metadata"][$type]['tableCount']);}
                if(!empty($file["metadata"][$type]["author"])){
                $xml->addNode('author', $file["metadata"][$type]['author']);}
                if(!empty($file["metadata"][$type]["isProtected"])){
                $xml->addNode('protected', $file["metadata"][$type]['isProtected']);}
                if(!empty($file["metadata"][$type]["software"])){
                $xml->addNode('software_name', $file["metadata"][$type]['software']);}
                if(!empty($file["metadata"][$type]["compressionScheme"])){
                $xml->addNode('compression', $file["metadata"][$type]['compressionScheme']);}
                if(!empty($file["metadata"][$type]["imageWidth"])){
                $xml->addNode('image_width', $file["metadata"][$type]['imageWidth']);}
                if(!empty($file["metadata"][$type]["imageHeight"])){
                $xml->addNode('image_height', $file["metadata"][$type]['imageHeight']);}
                if(!empty($file["metadata"][$type]["colorSpace"])){
                $xml->addNode('color_space', $file["metadata"][$type]['colorSpace']);}
                if(!empty($file["metadata"][$type]["bitsPerSample"])){
                $xml->addNode('bit_per_sample', $file["metadata"][$type]['bitsPerSample']);}
                if(!empty($file["metadata"][$type]["samplesPerPixel"])){
                $xml->addNode('sample_per_pixel', $file["metadata"][$type]['samplesPerPixel']);}
                if(!empty($file["metadata"][$type]["orientation"])){
                $xml->addNode('orientation', $file["metadata"][$type]['orientation']);}
                if(!empty($file["metadata"][$type]["lightSource"])){
                $xml->addNode('light_source', $file["metadata"][$type]['lightSource']);}
                if(!empty($file["metadata"][$type]["iccProfileName"])){
                $xml->addNode('icc_profile_name', $file["metadata"][$type]['iccProfileName']);}
                if(!empty($file["metadata"][$type]["iccProfileVersion"])){
                $xml->addNode('icc_profile_version', $file["metadata"][$type]['iccProfileVersion']);}
                if(!empty($file["metadata"][$type]["YCbCrSubSampling"])){
                $xml->addNode('ycbcr_sub_sampling', $file["metadata"][$type]['YCbCrSubSampling']);}
                if(!empty($file["metadata"][$type]["xSamplingFrequency"])){
                $xml->addNode('x_sampling_frequency', $file["metadata"][$type]['xSamplingFrequency']);}
                if(!empty($file["metadata"][$type]["ySamplingFrequency"])){
                $xml->addNode('y_sampling_frequency', $file["metadata"][$type]['ySamplingFrequency']);}
                if(!empty($file["metadata"][$type]["isTagged"])){
                $xml->addNode('is_tagged', $file["metadata"][$type]['isTagged']);}
                if(!empty($file["metadata"][$type]["hasOutline"])){
                $xml->addNode('has_outline', $file["metadata"][$type]['hasOutline']);}
                if(!empty($file["metadata"][$type]["hasAnnotations"])){
                $xml->addNode('has_annotation', $file["metadata"][$type]['hasAnnotations']);}
                if(!empty($file["metadata"][$type]["hasForms"])){
                $xml->addNode('has_forms', $file["metadata"][$type]['hasForms']);}
                if(!empty($file["metadata"][$type]["isRightsManaged"])){
                $xml->addNode('is_right_managed', $file["metadata"][$type]['isRightsManaged']);}
                if(!empty($file["metadata"][$type]["linebreak"])){
                $xml->addNode('line_break', $file["metadata"][$type]['linebreak']);}
                if(!empty($file["metadata"][$type]["charset"])){
                $xml->addNode('charset', $file["metadata"][$type]['charset']);}
                if(!empty($file["metadata"][$type]["duration"])){
                $xml->addNode('duration', $file["metadata"][$type]['duration']);}
                if(!empty($file["metadata"][$type]["bitDepth"])){
                $xml->addNode('bit_depth', $file["metadata"][$type]['bitDepth']);}
                if(!empty($file["metadata"][$type]["bitRate"])){
                $xml->addNode('bit_rate', $file["metadata"][$type]['bitRate']);}
                if(!empty($file["metadata"][$type]["frameRate"])){
                $xml->addNode('frame_rate', $file["metadata"][$type]['frameRate']);}
                if(!empty($file["metadata"][$type]["sampleRate"])){
                $xml->addNode('sample_rate', $file["metadata"][$type]['sampleRate']);}
                if(!empty($file["metadata"][$type]["dataFormatType"])){
                $xml->addNode('data_format_type', $file["metadata"][$type]['dataFormatType']);}
                if(!empty($file["metadata"][$type]["channels"])){
                $xml->addNode('channels', $file["metadata"][$type]['channels']);}
                if(!empty($file["metadata"][$type]["markupBasis"])){
                $xml->addNode('markup_basis', $file["metadata"][$type]['markupBasis']);}
                if(!empty($file["metadata"][$type]["markupBasisVersion"])){
                $xml->addNode('markup_basis_version', $file["metadata"][$type]['markupBasisVersion']);}
                if(!empty($file["metadata"][$type]["digitalCameraModelName"])){
                $xml->addNode('digital_camera_model_name', $file["metadata"][$type]['digitalCameraModelName']);}
                if(!empty($file["metadata"][$type]["fNumber"])){
                $xml->addNode('fnumber', $file["metadata"][$type]['fNumber']);}
                if(!empty($file["metadata"][$type]["exposureTime"])){
                $xml->addNode('exposure_time', $file["metadata"][$type]['exposureTime']);}
                if(!empty($file["metadata"][$type]["isoSpeedRating"])){
                $xml->addNode('iso_speed_rating', $file["metadata"][$type]['isoSpeedRating']);}
                if(!empty($file["metadata"][$type]["exposureBiasValue"])){
                $xml->addNode('exposure_bias_value', $file["metadata"][$type]['exposureBiasValue']);}
                if(!empty($file["metadata"][$type]["subjectDistance"])){
                $xml->addNode('subject_distance', $file["metadata"][$type]['subjectDistance']);}
                if(!empty($file["metadata"][$type]["meteringMode"])){
                $xml->addNode('metering_mode', $file["metadata"][$type]['meteringMode']);}
                if(!empty($file["metadata"][$type]["flash"])){
                $xml->addNode('flash', $file["metadata"][$type]['flash']);}
                if(!empty($file["metadata"][$type]["focalLength"])){
                $xml->addNode('focal_length', $file["metadata"][$type]['focalLength']);}
                if(!empty($file["metadata"][$type]["digitalCameraManufacturer"])){
                $xml->addNode('digital_camera_manufacturer', $file["metadata"][$type]['digitalCameraManufacturer']);}
                if(!empty($file["metadata"][$type]["exposureProgram"])){
                $xml->addNode('exposure_program', $file["metadata"][$type]['exposureProgram']);}
                if(!empty($file["metadata"][$type]["exifVersion"])){
                $xml->addNode('exif_version', $file["metadata"][$type]['exifVersion']);}
                if(!empty($file["metadata"][$type]["shutterSpeedValue"])){
                $xml->addNode('shutter_speed_value', $file["metadata"][$type]['shutterSpeedValue']);}
                if(!empty($file["metadata"][$type]["apertureValue"])){
                $xml->addNode('aperture_value', $file["metadata"][$type]['apertureValue']);}
                if(!empty($file["metadata"][$type]["maxApertureValue"])){
                $xml->addNode('max_aperture_value', $file["metadata"][$type]['maxApertureValue']);}
                if(!empty($file["metadata"][$type]["gpsVersionID"])){
                $xml->addNode('gps_version_id', $file["metadata"][$type]['gpsVersionID']);}
                if(!empty($file["metadata"][$type]["gpsLatitudeRef"])){
                $xml->addNode('gps_latitude_ref', $file["metadata"][$type]['gpsLatitudeRef']);}
                if(!empty($file["metadata"][$type]["gpsLatitude"])){
                $xml->addNode('gps_latitude', $file["metadata"][$type]['gpsLatitude']);}
                if(!empty($file["metadata"][$type]["gpsLongitudeRef"])){
                $xml->addNode('gps_longitude_ref', $file["metadata"][$type]['gpsLongitudeRef']);}
                if(!empty($file["metadata"][$type]["gpsLongitude"])){
                $xml->addNode('gps_longitude', $file["metadata"][$type]['gpsLongitude']);}
                if(!empty($file["metadata"][$type]["gpsAltitudeRef"])){
                $xml->addNode('gps_altitude_ref', $file["metadata"][$type]['gpsAltitudeRef']);}
                if(!empty($file["metadata"][$type]["gpsAltitude"])){
                $xml->addNode('gps_altitude', $file['gpsAltitude']);}
                if(!empty($file["metadata"][$type]["gpsTimeStamp"])){
                $xml->addNode('gps_timestamp', $file["metadata"][$type]['gpsTimeStamp']);}
                if(!empty($file["metadata"][$type]["gpsProcessingMethod"])){
                $xml->addNode('gps_processing_method', $file["metadata"][$type]['gpsProcessingMethod']);}
                if(!empty($file["metadata"][$type]["gpsDateStamp"])){
                $xml->addNode('gps_date_stamp', $file["metadata"][$type]['gpsDateStamp']);}
                if(!empty($file["metadata"][$type]["brightnessValue"])){
                $xml->addNode('brightness_value', $file["metadata"][$type]['brightnessValue']);}
                if(!empty($file["metadata"][$type]["rotation"])){
                $xml->addNode('rotation', $file["metadata"][$type]['rotation']);}
                if(!empty($file["metadata"][$type]["audioSampleRate"])){
                $xml->addNode('audio_sample_rate', $file["metadata"][$type]['audioSampleRate']);}

                

        // End branch 1
        $xml->endBranch();      
        

        // Print the XML to screen
        $xml->getXml(true);
    }

    function callxml($id)
    {
        
        $id = $this->uri->segment(3);
        $file_xml = $this->get_xml($id);

      // var_dump($id);exit();
    }


    function get_xml_bigdata($id)
    {
        
        $id = $this->uri->segment(3);
        // var_dump($id);exit();

        $file = $this->search_m->get_bigdata_by_id($id);
        

        $file = $file['big_data'];


        // echo "<pre>";var_dump($file);exit();
        


        // Load XML writer library
        $this->load->library('MY_Xml_Writer');

        // Initiate Class
        $xml = new MY_Xml_Writer;
        $xml->setRootName('BigData');
        $xml->Initiate();

        // Start branch 1
        $xml->startBranch('variety');

                if(!empty($file["variety"]["filename"])){
                $xml->addNode('nama_file', $file["variety"]['filename']);}
                if(!empty($file["variety"]["Mimetype"])){
                $xml->addNode('jenis_file', $file["variety"]['Mimetype']);}
                if(!empty($file["variety"]["Format"])){
                $xml->addNode('format', $file["variety"]['Format']);}
                if(!empty($file["variety"]["creatingApplicationName"])){
                $xml->addNode('creating_application_name', $file["variety"]['creatingApplicationName']);}
                if(!empty($file["variety"]["version"])){
                $xml->addNode('version', $file["variety"]['version']);}
                if(!empty($file["variety"]["compressionScheme"])){
                $xml->addNode('compression', $file["variety"]['compressionScheme']);}
            
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('volume');

                if(!empty($file["volume"]["size"])){
                $xml->addNode('size', $file["volume"]['size']);}
                if(!empty($file["volume"]["pageCount"])){
                $xml->addNode('page_count', $file["volume"]['pageCount']);}
                if(!empty($file["volume"]["wordcount"])){
                $xml->addNode('word_count', $file["volume"]['wordcount']);}
                if(!empty($file["volume"]["characterCount"])){
                $xml->addNode('character_count', $file["volume"]['characterCount']);}
                if(!empty($file["volume"]["graphicsCount"])){
                $xml->addNode('graphic_count', $file["volume"]['graphicsCount']);}
                if(!empty($file["volume"]["tableCount"])){
                $xml->addNode('table_count', $file["volume"]['tableCount']);}
                if(!empty($file["volume"]["imageWidth"])){
                $xml->addNode('image_width', $file["volume"]['imageWidth']);}
                if(!empty($file["volume"]["imageHeight"])){
                $xml->addNode('image_height', $file["volume"]['imageHeight']);}
                if(!empty($file["volume"]["bitsPerSample"])){
                $xml->addNode('bit_per_sample', $file["volume"]['bitsPerSample']);}
                if(!empty($file["volume"]["samplesPerPixel"])){
                $xml->addNode('sample_per_pixel', $file["volume"]['samplesPerPixel']);}
                if(!empty($file["volume"]["duration"])){
                $xml->addNode('duration', $file["volume"]['duration']);}
                if(!empty($file["volume"]["bitDepth"])){
                $xml->addNode('bit_depth', $file["volume"]['bitDepth']);}
                if(!empty($file["volume"]["bitRate"])){
                $xml->addNode('bit_rate', $file["volume"]['bitRate']);}
                if(!empty($file["volume"]["frameRate"])){
                $xml->addNode('frame_rate', $file["volume"]['frameRate']);}
                if(!empty($file["volume"]["sampleRate"])){
                $xml->addNode('sample_rate', $file["volume"]['sampleRate']);}
                if(!empty($file["volume"]["channels"])){
                $xml->addNode('channels', $file["volume"]['channels']);}
 
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('time');

                if(!empty($file["time"]["lastmodified"])){
                $xml->addNode('last_modified', $file["time"]['lastmodified']);}
                if(!empty($file["time"]["created"])){
                $xml->addNode('created', $file["time"]['created']);}
                
                
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('value');

                if(!empty($file["value"]["filename"])){
                $xml->addNode('filename', $file["value"]['filename']);}
                if(!empty($file["value"]["author"])){
                $xml->addNode('author', $file["value"]['author']);}
                if(!empty($file["value"]["title"])){
                $xml->addNode('title', $file["value"]['title']);}
                if(!empty($file["value"]["creator"])){
                $xml->addNode('creator', $file["value"]['creator']);}
                if(!empty($file["value"]["kategori"])){
                $xml->addNode('kategori', $file["value"]['kategori']);}
                if(!empty($file["value"]["description"])){
                $xml->addNode('description', $file["value"]['description']);}
                
                
        // End branch 1
        $xml->endBranch();



        $xml->startBranch('veracity');

                if(!empty($file["veracity"]  ["md5checksum"])){
                $xml->addNode('md5checksum', $file["veracity"]  ["md5checksum"]);}
                if(!empty($file["veracity"]["well_formed"])){
                $xml->addNode('well_formed', $file["veracity"]['well_formed']);}
                if(!empty($file["veracity"]["message"])){
                $xml->addNode('message', $file["veracity"]['message']);}
                if(!empty($file["veracity"]["valid"])){
                $xml->addNode('valid', $file["veracity"]['valid']);}
                if(!empty($file["veracity"]["isProtected"])){
                $xml->addNode('protected', $file["veracity"]['isProtected']);}
                if(!empty($file["veracity"]["isRightsManaged"])){
                $xml->addNode('is_right_managed', $file["veracity"]['isRightsManaged']);}
                

                
        // End branch 1
        $xml->endBranch();

        $xml->startBranch('velocity');

                if(!empty($file["velocity"]["fitsExecutionTime"])){
                $xml->addNode('fitsExecutionTime', $file["velocity"]['fitsExecutionTime']);}
                

                
        // End branch 1
        $xml->endBranch();   
        
    // Print the XML to screen
        $xml->getXml(true);
    }

    function callxml_bigdata($id)
    {

        $id = $this->uri->segment(3);
        $file_xml = $this->get_xml_bigdata($id);

        // var_dump($id);exit();

    }



}
