<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url','text']);
        $this->load->model('search_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {
        $q = $this->input->get('q');
        $q2 = $this->input->get('q2');
        
        $data =['q' => $q,
                'q2' => $q2];

        $data['file'] =array();

        if($this->input->get('btncari')){
          
          if ($q) 
          { 
              $data['file'] = $this->search_m->search();
          } 
          else 
          {
              $data['file'] = $this->search_m->get_all_file(); 
          }
        }

        $FT = array(); 
        if($this->input->get('document')=="on") $FT[] = "document";
        if($this->input->get('video')=="on") $FT[] = "video";
        if($this->input->get('image')=="on") $FT[] = "image";
        if($this->input->get('audio')=="on") $FT[] = "audio";
        // echo '<pre>';var_dump($FT);echo '</pre>';exit;
        if(count($FT)>0){
          $data['file_baru']=array();
          foreach($data['file'] as $k => $v){
               foreach($v['metadata'] as $kw1 => $nil){
                  foreach($FT as $b){
                     if($kw1==$b){
                        $data['file_baru'][]=$v;
                     }
                  }
              }
          }
          $data['file']=$data['file_baru'];
     
        }

        $FS = array(); 
        if($this->input->get('wellformed')=="on") $FS[] = "well-formed";
        if($this->input->get('valid')=="on") $FS[] = "valid";

 
        if(count($FS)>0){
          $data['file_baru']=array();
          foreach($data['file'] as $k => $v){
               foreach($v['filestatus'] as $kw1 => $nil){
                 foreach($FS as $b){
                     $sint=0;
                     if($kw1==$b && $nil==true){
                        $sint=1;
                     }
                  }
                  if($sint==1){
                     $data['file_baru'][]=$v;
                     $sint=0;
                  }
              }
          }
          $data['file']=$data['file_baru'];
        }

        $FC = array(); 
        if($this->input->get('architecture')=="on") $FC[] = "Architecture";
        if($this->input->get('arts')=="on") $FC[] = "Arts";
        if($this->input->get('biology')=="on") $FC[] = "Biology";
        if($this->input->get('chemistry')=="on") $FC[] = "Chemistry";
        if($this->input->get('communications')=="on") $FC[] = "Communications";
        if($this->input->get('computerscience')=="on") $FC[] = "Computer Science";
        if($this->input->get('economics')=="on") $FC[] = "Economics";
        if($this->input->get('engineering')=="on") $FC[] = "Engineering";
        if($this->input->get('forestry')=="on") $FC[] = "Forestry";
        if($this->input->get('geography')=="on") $FC[] = "Geography";
        if($this->input->get('history')=="on") $FC[] = "History";
        if($this->input->get('law')=="on") $FC[] = "Law";
        if($this->input->get('mathematics')=="on") $FC[] = "Mathematics";
        if($this->input->get('physics')=="on") $FC[] = "Physics";
        if($this->input->get('politics')=="on") $FC[] = "Politics";
        if($this->input->get('psychology')=="on") $FC[] = "Psychology";
 
        if(count($FC)>0){
          $data['file_baru']=array();
          foreach($data['file'] as $k => $v){
               foreach($FC as $b){
                 if($v['category']==$b){
                    $data['file_baru'][]=$v;
                 }
              }
          }
          $data['file']=$data['file_baru'];
        }

        $data['title'] = 'Search Your File';
        $data['page'] = 'search_v';
        $this->load->view('templates/container',$data);
        
    }

    

    function result($id)
    {
        $$id = $this->uri->segment(3);
        $details = $this->search_m->get_file_by_id($id);
        // echo "<pre>";var_dump($details);exit;
        $mapping = $this->search_m->get_bigdata_by_id($id);
        // $idku =  $mapping['idmeta'];
        // echo "<pre>";var_dump($mapping);exit;
      
    
        $image = array('image/jpeg', 'image/png', 'image/gif','image/jpg','image/tiff', 'image/bmp','Application/vnd.oasis.opendocument.text','Image/vnd.adobe.photoshop');
        $video = array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska');
        $audio = array('audio/mpeg', 'audio/wav','audio/mp3','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts');
        $viewerjs = array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'application/vnd.ms-excel',
                     'video/webm','application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {

            $data['detail'] = [
                '_id' => $detail["_id"],
                'title' => $detail["title"],
                'creator' => 'Creator : '.$detail["creator"],
                'category' => 'Category : '.$detail["category"],
                'description' => $detail["description"],
                'identification' => $detail["identification"],
                'fileinfo' => $detail["fileinfo"],
                'filestatus' => $detail["filestatus"],
                'statistics'=>$detail["statistics"],
                'metadata' => $detail["metadata"],
                'comments' => $detail["comments"]
            ];
            // echo "<pre>";var_dump($data['detail']);exit;
            if (in_array($detail['identification']['Mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $viewerjs))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/else';
                $data['details'] = $details;               
            }
            
            }
            // echo "<pre>";var_dump($mapping['big_data']);exit;
            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                        // 'time' => $mapping['big_data']['time']
                    ];
                    // var_dump($mapping['big_data']['value']);exit;
                 if (in_array($mapping['big_data']['variety']['Mimetype'], $image))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/image';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $video))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/video';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $audio))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/audio';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $viewerjs))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/viewerjs';
                    $data['details'] = array($mapping);
                }
                else
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/else';
                    $data['details'] = array($mapping);               
                }
            $this->load->view('templates/container',$data);
}

}  
