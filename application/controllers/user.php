<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->library('form_validation');
        $this->load->helper('text');
        $this->load->model('user_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {
        $users = $this->user_m->get_all_user();
        if(!empty($users))
        {
            foreach ($users as $user) 
            {
                $data['users'][] = [
                    'id' => $user['_id']->__toString(),
                    'fullname'=>$user['fullname'],
                    'email'=>$user['email'],
                    'username'=>$user['username'],
                    'joined_at' => $user['joined_at']
                ];
            }
        }
        $data['pesan_sukses'] = $this->session->flashdata('pesan_sukses');
        $data['title'] = 'List User';
        $data['page'] = 'user_v';
        $this->load->view('templates/container',$data);
    }

    function upload_pp()
    {
        $config['upload_path'] = './uploads/tumb/';
        $config['allowed_types'] = 'gif|jpg|png';

        $this->load->library('upload',$config);

        if(!$this->upload->do_upload())
        {
            //if there is any error
            $error = ['error' => $this->upload->display_errors()];
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
        }
    }


    public function create_user() 
    {
        $this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|xss_clean|min_length[4]|max_length[12]|');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) 
        {
            redirect('user');
        } 
        else 
        {   
            
            $user = array(
                'joined_at' => new MongoDate(),
                'fullname' => $this->input->post('fullname'),
                'email'    => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password')),
                'role'     => 'user'
            );
            $result = $this->user_m->create_user($user);
            $this->session->set_flashdata('pesan_sukses','<div class="alert alert-success"><a class="close">&times;</a>You have successfully added a new member!.</div>');
            redirect('user');
        }
    }

    function update_user()
    {
        $id = $this->uri->segment(3);
        $data['users']=$this->user_m->get_user_by_id($id);
        
        if(count($data['users']) == 0)
        {
            show_404();
        }
        foreach ($data['users'] as $user) 
        {
            $data['user'] = [
            'id' => $user['_id'],
            'fullname'=>$user['fullname'],
            'email'=>$user['email'],
            'username'=>$user['username'],
            ];

        }
        
        if ($this->input->post('btnSimpan'))
        {
            $u=[
                'fullname' => $this->input->post('fullname'),
                'email' => $this->input->post('email'),
                'username' => $this->input->post('username'),
                'password'=> $this->input->post('password')
            ];

            $this->user_m->update_user($id,$u);
            $this->session->set_flashdata('pesan_sukses','<div class="alert alert-success"><a class="close">&times;</a>You have successfully update one member!.</div>');
            redirect('user');
        }
        $data['title'] = 'Edit File';
        $data['page'] = 'updateuser_v';
        $this->load->view('templates/container',$data);
    }

    function view_user($id)
    {
        $id = $this->uri->segment(3);
        $users=$this->user_m->get_user_by_id($id);

        if(count($users) == 0)
        {
            show_404();
        }
        foreach ($users as $user) 
        {
            $data['user'] = [
                'id' => $user['_id'],
                'fullname' => $user['fullname'],
                'email' => $user['email'],
                'username' => $user['username'],
                'joined_at' => $user['joined_at']
                ];
        }
        $data['title'] = 'Your File';
        $data['page'] = 'profile_v';
        $this->load->view('templates/container',$data);
    }

    function delete_user($id)
    {
        $this->user_m->delete_by_id($id);
        $this->session->set_flashdata('pesan_sukses','<div class="alert alert-success"><a class="close">&times;</a>You have successfully delete one member!.</div>');
        redirect('user/','refresh');
    }
}