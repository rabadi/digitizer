<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('search_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {

        // $results = $this->search_m->get_all_file();
        $results = $this->search_m->get_all_bigdata();
        // var_dump($results);exit();
        if(!empty($results))
        {
            foreach ($results as $result) 
            {
                // var_dump($result);exit;
                $data['results'][] = [
                    'id' => $result['_id']->__toString(),
                    'idmeta' => $result['idmeta'],
                    'name'=>$result['big_data']['value']['filename'],
                    'creator' => $result['big_data']['value']['creator'],
                    'format' => $result['big_data']['variety']['Mimetype'],
                    'time' => $result['big_data']['time']['lastmodified'],
                    'volume' => $result['big_data']['volume']['size'],
                    'description' => $result['big_data']['value']['description']
                    // 'uploaded_at'=> $result['uploaded_at']
                ];
                // exit(var_dump($data));
            }
        }

        // usort($data['results'], function($a, $b) {
        //     return strtotime(date('m/d/Y h:i:s', $b['time']->sec)) - strtotime(date('m/d/Y h:i:s', $a['time']->sec));
        // });

        // if(!empty($result1))
        // {
        //     foreach ($result1 as $results1) 
        //     {
        //         $data['results'][] = [
        //             'volume' => $results1['big_data']['volume']['size'],
        //             'category'=> $results1['big_data']['value']['kategori']
        //         ];
        //     }
        // // }
        // echo "<pre>";
        // var_dump($data);
        // echo "</pre>";
        // exit();
        $data['title'] = 'Your File';
        $data['page'] = 'allfiles_v';
        $this->load->view('templates/container',$data);
    }

    function sortFunction( $a, $b ) 
    {   

        return strtotime($a[1]) - strtotime($b[1]);
    }

    function user_file()
    {
        $results = $this->search_m->get_user_file();
        if(!empty($results))
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                    'id' => $result['_id']->__toString(),
                    'name'=>$result['title'],
                    'uploaded_at'=>$result['uploaded_at'],
                ];
            }
        }
        $data['title'] = 'Your File';
        $data['page'] = 'file_v';
        $this->load->view('templates/container',$data); 
    }

    function delete_userfile($id)
    {
        $this->search_m->delete_by_id($id);
        redirect('file/user_file','refresh');
    }

    function detail($id)
    {
        $id = $this->uri->segment(3);
        $details = $this->search_m->get_file_by_id($id);
        $mapping = $this->search_m->get_bigdata_by_id($id);
     
    
        $image = array('image/jpeg', 'image/png', 'image/gif','image/jpg','image/tiff', 'image/bmp','Application/vnd.oasis.opendocument.text','Image/vnd.adobe.photoshop');
        $video = array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska');
        $audio = array('audio/mpeg', 'audio/wav','audio/mp3','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts');
        $viewerjs = array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'application/vnd.ms-excel',
                     'video/webm','application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {

            $data['detail'] = [
                '_id' => $detail["_id"],
                'title' => $detail["title"],
                'creator' => 'Creator : '.$detail["creator"],
                'category' => 'Category : '.$detail["category"],
                'description' => $detail["description"],
                'identification' => $detail["identification"],
                'fileinfo' => $detail["fileinfo"],
                'filestatus' => $detail["filestatus"],
                'statistics'=>$detail["statistics"],
                'metadata' => $detail["metadata"],
                'comments' => $detail["comments"]
            ];
           
            if (in_array($detail['identification']['Mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $viewerjs))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/else';
                $data['details'] = $details;               
            }
            
            }
           

            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                    ];
                   
                 if (in_array($mapping['big_data']['variety']['Mimetype'], $image))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/image';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $video))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/video';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $audio))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/audio';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $viewerjs))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/viewerjs';
                    $data['details'] = array($mapping);
                }
                else
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/else';
                    $data['details'] = array($mapping);               
                }
            $this->load->view('templates/container',$data);
    }   
 

    function delete_file($id)
    {
        $this->search_m->delete_by_id($id);
        $this->search_m->delete_bigdata_by_id($id);
        redirect('datamart/','refresh');
    }

    function update_file()
    {
        $id = $this->uri->segment(3);
        $data['details']= $this->search_m->get_file_by_id($id);
        // var_dump($data['details']);exit;
        if(count($data['details']) == 0)
        {
            show_404();
        }
        foreach ($data['details'] as $detail) 
        {
            $data['detail'] = [
                'title' => $detail["title"]. ' '. ' Characteristics',
                'identification' => $detail["identification"],
                'fileinfo' => $detail["fileinfo"],
                'filestatus' => $detail['filestatus'],
                'statistics' => $detail['statistics'],
                'metadata' => $detail["metadata"],
            ];
        }

        if($this->input->post('btnSimpan')){
            
            $data1 = array();
            $data1['title'] = $this->input->post('title');
            $data1['creator'] = $this->input->post('creator');
            $data1['category'] = $this->input->post('category');
            $data1['description'] = $this->input->post('description');

            $this->search_m->update_data($id, $data1);
            $this->search_m->update_bigdata($id, $data1); 
            redirect('file/detail/'.$id);
        }
        // var_dump($data);exit;

        $data['title'] = 'Edit File';
        $data['page'] = 'edit_v';
        $this->load->view('templates/container',$data);
    }


    function insert_comment()    
    {
        $id = $this->uri->segment(3); 
 
        if($this->input->post('btnComment')){
            $comment = array(
                'user_id' => $this->session->userdata('user_id'),
                'fullname' => $this->session->userdata('fullname'),
                'des_comment' => $this->input->post('des_comment'),
                'date' => new MongoDate()
                );
        
        $this->search_m->insert_comment($id,$comment); 
        redirect('file/detail/'.$id);
        }

    }

}
