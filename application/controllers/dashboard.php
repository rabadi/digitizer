<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('search_m');
        $this->load->model('count_m'); $this->load->library('pagination');
       
    }

    public function index()
    {

        // $results = $this->search_m->get_all_file();
         $q = $this->input->get('q');
        $q2 = $this->input->get('q2');
        
        $data =['q' => $q,
                'q2' => $q2];
                if($this->input->get('q') || $this->input->get('btncari'))
        {
            $results = $this->search_m->search_bigdata(0,$this->uri->segment(3)); 
            $jml_data = (sizeof($results));
        }else{
            $jml_data=$this->search_m->count_data();
        }
        
        $data['datacount'] = (int)$jml_data;
        $config = array();
        $config["base_url"] = site_url('dashboard/index');
        $config["total_rows"] = $jml_data;
        $config["per_page"] = 5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';  
        $config['num_tag_close'] = '</li>';        
        $config['uri_segment'] = 3;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';

        $this->pagination->initialize($config);
        $data['str_links'] = $this->pagination->create_links();
        // $data['links'] = explode('&nbsp;',$str_links );
        $results = $this->search_m->get_all_bigdata($config["per_page"],$this->uri->segment(3));
        if($this->input->get('q') || $this->input->get('btncari'))
        {
              $results = $this->search_m->search_bigdata($config["per_page"],$this->uri->segment(3)); 
        }
        $data['results'] = array();

        //jml image
        $jml_img=$this->search_m->count_image();
             $data['imagecount'] = (string)$jml_img;
        //jml dokumen
        $jml_doku=$this->search_m->count_dokumen();
            $data['dokumencount'] = (string)$jml_doku;
        //jml audio
        $jml_audio=$this->search_m->count_audio();
            $data['audiocount']=(string)$jml_audio;
        //jml video
        $jml_video=$this->search_m->count_video();
            $data['videocount']=(string)$jml_video;

        //jml category
         $jml_cs=$this->count_m->count_cs();
        $data['computerscience']=(int)$jml_cs;
         $jml_architect=$this->count_m->count_architect();
        $data['architecturecount']=(int)$jml_architect;
        $jml_art=$this->count_m->count_art();
            $data['arts']=(int)$jml_art;
        $jml_bio=$this->count_m->count_bio();
            $data['biologycount']=(int)$jml_bio;
        $jml_kim = $this->count_m->count_kimia();
            $data['kimiacount']= (int)$jml_kim;
        $jml_komunikasi = $this->count_m->count_komunikasi();
            $data['komcount']= (int)$jml_komunikasi;
         $jml_economy = $this->count_m->count_eco();
            $data['ecocount']= (int)$jml_economy;
        $jml_engine = $this->count_m->count_engineering();
            $data['enginecount']= (int)$jml_engine;
        $jml_geo = $this->count_m->count_geo();
            $data['geocount']= (int)$jml_geo;
        $jml_history = $this->count_m->count_history();
            $data['historycount']= (int)$jml_history;
        $jml_law = $this->count_m->count_law();
            $data['lawcount']= (int)$jml_law;
        $jml_math = $this->count_m->count_mathematics();
            $data['mathcount']= (int)$jml_math;
        $jml_physics = $this->count_m->count_physics();
            $data['physicscount']= (int)$jml_physics;
        $jml_politics = $this->count_m->count_politics();
            $data['politicscount']= (int)$jml_politics;
        $jml_psychology = $this->count_m->count_psychology();
            $data['psychologycount']= (int)$jml_psychology;
       $jml_data=$this->search_m->count_data();
            $data['datacount'] = (string)$jml_data;

      

         // $data['file'] =array();
        // if($this->input->get('q') || $this->input->get('btncari')){

        //       $results = $this->search_m->search_bigdata($config["per_page"],$this->uri->segment(3)); 
        // } 

         if(count($results)>0)
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                'id' => $result['_id']->__toString(),
                'idmeta' => $result['idmeta'],
                // 'name'=>$result['big_data']['value']['title'],
                'title' => $result['big_data']['value']['tittle'],
                'filename' => $result['big_data']['value']['filename'],
                'creator' => $result['big_data']['value']['creator'],
                'format' => $result['big_data']['variety']['Mimetype'],
                'time' => $result['big_data']['time']['lastmodified'],
                'velocity' => $result['big_data']['velocity']['fitsExecutionTime'],
                'veracity' =>  (!empty($result['big_data']['veracity']['well-formed'])) ?
                     ($result['big_data']['veracity']['well-formed']) : null,
                'volume' => $result['big_data']['volume']['size'],
                'category' => $result['big_data']['value']['category'],
                'description' => $result['big_data']['value']['description']
                
                ];

            }
        }


        $data['title'] = 'B-mart Dashboard';
        $data['page'] = 'dashboard_v';
        $this->load->view('templates/dashboard',$data);
        
    }

    function search_result($id)
    {
        
        $id = $this->uri->segment(3);
        // $filename = $this->uri->segment(4);
        $data['id'] = $id;
        // var_dump($id);exit();

        $details = $this->search_m->get_file_by_id($id);
        $mapping = $this->search_m->get_bigdata_by_id($id);
        
        // $data['data_xml'] = "uploads/extracted_data/".$filename;

        // echo'<pre>';var_dump($details);exit();
        // echo "aaaaa";
        // exit();

       $image = array('image/jpeg', 'image/png', 'image/gif','image/jpg','image/tiff', 'image/bmp','Application/vnd.oasis.opendocument.text','Image/vnd.adobe.photoshop');
        $video = array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska');
        $audio = array('audio/mpeg', 'audio/wav','audio/mp3','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts');
        $viewerjs = array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'application/vnd.ms-excel',
                     'video/webm','application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text');

        if(count($details) == 0)
        {
            show_404();
        }
        foreach ($details as $detail) 
        {

            $data['detail'] = [
            '_id' => $detail["_id"],
            'title' => $detail["title"],
            'creator' => 'Creator : '.$detail["creator"],
            'category' => 'Category : '.$detail["category"],
            'description' => $detail["description"],
            'identification' => $detail["identification"],
            'fileinfo' => $detail["fileinfo"],
            'filestatus' => $detail["filestatus"],
            'statistics' => $detail["statistics"],
            'metadata' => $detail["metadata"],
            'comments' => $detail["comments"]
            ];

            if (in_array($detail['identification']['Mimetype'], $image))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/image';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $video))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/video';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $audio))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/audio';
                $data['details'] = $details;
            }
            elseif (in_array($detail['identification']['Mimetype'], $viewerjs))
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/viewerjs';
                $data['details'] = $details;
            }
            else
            {
                $data['title'] = 'Your File';
                $data['page'] = 'details/else';
                $data['details'] = $details;               
            }
        }    

            $data['detil'] = [
                        'variety' => $mapping['big_data']['variety'],
                        'volume' => $mapping['big_data']['volume'],
                        'veracity' => $mapping['big_data']['veracity'],
                        'velocity' => $mapping['big_data']['velocity'],
                        'value' => $mapping['big_data']['value'],
                        // 'time' => $mapping['big_data']['time']
                    ];

                // var_dump($mapping['big_data']['value']);exit;
                if (in_array($mapping['big_data']['variety']['Mimetype'], $image))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/image';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $video))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/video';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $audio))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/audio';
                    $data['details'] = array($mapping);
                }
                elseif (in_array($mapping['big_data']['variety']['Mimetype'], $viewerjs))
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/viewerjs';
                    $data['details'] = array($mapping);
                }
                else
                {
                    $data['title'] = 'Your File';
                    $data['page'] = 'details/else';
                    $data['details'] = array($mapping);               
                }

            $this->load->view('templates/container',$data);
        
      }  
}