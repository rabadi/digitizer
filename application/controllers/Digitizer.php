<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Digitizer extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('file_m');
    }

    function index(){
    	$results= $this->file_m->get_file();
    	if(!empty($results)){
    		foreach ($results as $result) {
    			 $data['results'][] = [
                    'id' => $result['_id']->__toString(),
                    'filename' => $result['upload_data']['raw_name']
                  ];
    		}
    	}

    	$data['title'] = 'Digitizer';
        $data['page'] = 'upload_form';
        $this->load->view('templates/dashboard',$data);
    }


     function do_upload()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = '*';
	

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			// echo "<pre>";var_dump($data);exit;
			// $this->load->view('upload_succes.php', $data);
		}

		$this->mongo_db->insert('lasfile', $data); 

		$data['title'] = 'Digitizer';
        $data['page'] = 'upload_succes';
        $this->load->view('templates/dashboard',$data);
	}

	function readcsv($filename){
		$filename = $this->uri->segment(3);

		$row = 1;
		if (($handle = fopen("./uploads/".$filename.".csv", "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		        $num = count($data);
		        echo "<p> $num fields in line $row: <br /></p>\n";
		        $row++;
		        for ($c=0; $c < $num; $c++) {
		            echo $data[$c] . "<br />\n";
		        }
		    }
		    fclose($handle);
		}
	}

	function parsing_csv(){
		$filename = $this->uri->segment(3);

		$csv = new parseCSV();
		$csv->auto("./uploads/".$filename.".csv");
		$csv_parse = $csv->data;
		// $coba='aaaa';
		echo "<pre>";print_r($csv_parse);
		

		$data['title'] = 'Digitizer';
        $data['page'] = 'readcsv_v';
        $this->load->view('templates/dashboard',$data);
        return $csv_parse;
	}
}