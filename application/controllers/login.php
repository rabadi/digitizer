<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

session_start();

class Login extends CI_Controller 
{

	function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->library('form_validation');
        $this->load->helper('text');
        $this->load->model('user_m');

    }

    public function index()
    {
        $data['title'] = 'Login';
        $this->load->view('login_v');
    }


    //check for user login process
    public function do_login() 
    {
    	$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

    	if ($this->form_validation->run() == FALSE) 
        {
            $this->index();
    	} 
        else 
        {
            $user = $this->input->post('username');
            $pass = md5($this->input->post('password'));
            $user_login = $this->user_m->cek_login($user,$pass);

            if (! $user_login)
            {
                // login failed
                $this->session->set_flashdata('pesan_login',' <div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>Password atau Username yang anda masukkan salah!</div>');
                redirect('login');
            }
            else
            {
                // login success
                $this->session->set_userdata([
                                    'is_login' => TRUE,
                                    'user_id'  => $user_login[0]['_id'],
                                    'username' => $user_login[0]['username'],
                                    'fullname' => $user_login[0]['fullname'],
                                    'role'     => $user_login[0]['role']
                                    ]);
                redirect ('datamart');
            }
        }
    }

    // logout from admin page
    function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}