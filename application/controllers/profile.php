<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form','url']);
        $this->load->helper('text');
        $this->load->model('user_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {

        if(!empty($results))
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                    'id' => $result["_id"]->__toString(),
                    'fullname'=>$result["fullname"],
                    'email'=>$result["email"],
                ];
            }
        }

        $data['title'] = 'Your Profile';
        $data['page'] = 'profile_v';
        $this->load->view('templates/container',$data);
    }

    function delete_file($id)
    {
        $this->user_m->delete_by_id($id);
    }

}
