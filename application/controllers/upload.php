<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(['form', 'url']);
        $this->load->model('search_m');
        if ($this->session->userdata('is_login')!= TRUE){
            redirect ('login');
        }
    }

    public function index()
    {
        $results = $this->search_m->get_new_file();
        if(!empty($results))
        {
            foreach ($results as $result) 
            {
                $data['results'][] = [
                    'id' => $result['_id']->__toString(),
                    'name'=>$result['title'],
                    'uploaded_at'=>$result['uploaded_at'],
                ];
            }
        }
        $data['title'] = 'Upload Your File';
        $data['page'] = 'upload_v';
        $this->load->view('templates/container',$data);
    }

    public function do_upload()
    {
        $config['upload_path'] = './uploads/raw_data/';
        $config['allowed_types'] = '*';

        $this->load->library('upload',$config);

        if(!$this->upload->do_upload())
        {
            //if there is any error
            $error = ['error' => $this->upload->display_errors()];
            var_dump($error);

        }
        else
        {

            $data = ['upload_data' =>$this->upload->data()];
            // exit(var_dump($data));
            $data_path = $data['upload_data']['file_name'];

            $input_file = $data['upload_data']['full_path'];
            $extracted_path = 'C:/xampp/htdocs/rain/uploads/extracted_data/';
            $data_name = $data['upload_data']['raw_name']; 
            $output_file = $extracted_path.$data_name.'.xml';

            //command to extract metadata
            $command='C:/Fits/fits-0.8.4/fits.bat -i '.$input_file.' -o '.$output_file;
            exec($command);
            $this->xmltoarray($output_file,$data['upload_data']['file_type']);
        }
    }

    public function xmltoarray($filename="", $file_type)
    {   
      $id = $this->uri->segment(3);
        $xmlparser = xml_parser_create();
        $fp = fopen($filename , 'r');
        $xmldata = fread($fp, 4096);
        xml_parser_set_option($xmlparser, XML_OPTION_CASE_FOLDING, 0);
        xml_parse_into_struct($xmlparser, $xmldata, $values);

               //get data dari ekstraksi file upload
        
        $meta['uploaded_at']= new MongoDate();
        $meta['user_id']=$this->session->userdata('user_id');
        $meta['title']='';
        $meta['creator']='';
        $meta['category']='';
        $meta['description']='';
        $meta['identification']=array();
        $meta['fileinfo']=array();
        $meta['filestatus']=array();
        $meta['statistics']=array();
        $meta['metadata']=array();
        $meta['comments']=array();


        $type_meta="";
        $reformat = [];
        foreach ($values as  $key=>$v1) 
        {
            if($v1['tag']=='identity' && isset($v1['attributes'])) 
            {
                if(count($v1['attributes']) >0)
                {
                    $meta['identification'] = array(
                        "Format"=>$v1['attributes']['format'],
                        "Mimetype"=>$v1['attributes']['mimetype']);     

                }
 
            }
            if($key>0)
            { 
                if($values[$key-1]['tag'] == "fileinfo")
                {
                    if(isset($v1['value']))
                    {
                        if(trim($v1['value'])!="")
                        $meta['fileinfo']=array_merge($meta['fileinfo'],
                            array($v1['tag']=>$v1['value']));

                    } 
                        
                }
                if($values[$key-1]['tag'] == "filestatus")
                {
                    if(isset($v1['value']))
                    {
                        if(trim($v1['value'])!="")
                        $meta['filestatus']=array_merge($meta['filestatus'],
                            array($v1['tag']=>$v1['value'])); 

                    } 
                }

                if($v1['tag']=='statistics' && isset($v1['attributes']))
                {
                         if(count($v1['attributes']) >0)
                    {
                        $meta['statistics'] = array(
                            "fitsExecutionTime"=>$v1['attributes']['fitsExecutionTime']);             
                    } 
                }
                if($type_meta=="" && $v1['tag'] == "metadata")
                {
                    $type_meta = $values[$key+1]['tag'];
                    $meta['metadata'][$type_meta]=array();        
                }
                if($values[$key-1]['tag'] == $type_meta)
                {
                    if(isset($v1['value']))
                    {
                        if(trim($v1['value'])!="")
                        $meta['metadata'][$type_meta]=array_merge($meta['metadata'][$type_meta],
                            array($v1['tag']=>$v1['value']));

                    }

                }               
            }
  
        }  
        // var_dump($meta);exit;
        $file_types = [
        'image/png' => 'image',
        'image/jpg' => 'image',
        'image/jpeg' => 'image',
        'image/gif' => 'image',
        'image/tiff' => 'image',
        'image/bmp' => 'image',
        'Application/vnd.oasis.opendocument.text' => 'image',
        'image/vnd.adobe.photoshop' => 'image',
        'application/octet-stream' => 'image',
        'audio/mp3'=>'audio',
        'audio/mpeg' => 'audio',
        'audio/x-ms-wma' => 'audio',
        'audio/x-wave' => 'audio',
        'audio/wav' => 'audio',
        'audio/x-hx-aac-adts' => 'audio',
        'audio/mp4' => 'audio',
        'audio/x-m4a'=> 'audio',
        'audio/vnd.dlna.adts' => 'audio',
        'application/pdf' => 'document',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'document',
        'application/msword' => 'document',
        'application/vnd.ms-excel' => 'document',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'document',
        'video/webm' => 'document',
        'application/vnd.ms-powerpoint' => 'document',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'document',
        'application/vnd.oasis.opendocument.text' => 'document',
        'video/mp4' => 'video',
        'video/3gpp' => 'video',
        'video/mpeg' => 'video',
        'video/x-flv' => 'video',
        'video/x-ms-wmv' => 'video',
        'video/webm' => 'video',
        'video/x-matroska' => 'video'
        ];
        // var_dump($file_type);exit;
        // mennetukan tipe file
        if (array_key_exists($file_type, $file_types)) {
            $db = new MongoClient("localhost");
            $n = $db->selectDB('rain');
            $collection = new MongoCollection($n, 'match');

            $matching =  array(
                array('$match' => array("file_type" => $file_types[$file_type])
                  ));
            $cursor = $collection->aggregate($matching);
            
           // bandingkan file type yang sudah ada dengan mime type file
            $categories = [];
            foreach ($cursor as $doc) {
                foreach ((array)$doc as $k) {
                    array($categories[$k['obj_param']]=  $k['category']);
               }
           }
           
        }
        else {
            show_error('Jenis file tidak ditemukan.');
        }
        // echo "<pre>";var_dump($categories);exit;

        //reformat metadata to bigdata paramaeter
        foreach ($meta['identification'] as $key => $val) {
                if (array_key_exists($key, $categories)) {
                    $reformat[$categories[$key]][$key] = $val;
                    // var_dump($categories);exit;
                }
            }  
                  
        foreach ($meta['fileinfo'] as $key => $val) {
                if (array_key_exists($key, $categories)) {
                    $reformat[$categories[$key]][$key] = $val;
                }
           } 
            
        foreach ($meta['filestatus'] as $key => $val) {
                        if (array_key_exists($key, $categories)) {
                    $reformat[$categories[$key]][$key] = $val;

                        }
                   }
        foreach ($meta['statistics'] as $key => $val) {
                    if (array_key_exists($key, $categories)) {
                    $reformat[$categories[$key]][$key] = $val;


                    }
                }     
            foreach ($meta['metadata'][$type_meta]as $key => $val) {
                    if (array_key_exists($key, $categories)) {
                    $reformat[$categories[$key]][$key] = $val;

                    }
               } 

        // echo "<pre>";var_dump($reformat);exit;
        $this->mongo_db->insert('meta_collab', $meta); 
        $idmeta = (string)$meta['_id'];
        $upload_time =$meta['uploaded_at'];
        $getmapping = array(
            'big_data' => $reformat,
            'idmeta' =>$idmeta,
            'upload_at'=>$upload_time);
        $this->session->set_userdata('filemapping', $getmapping);
        $this->mongo_db->insert('bigdata', $getmapping); 
        $this->save();
    }
    
  
    public function save() {
        $results = $this->search_m->get_all_file();
        if(count($results)>0)
        { 
            foreach ($results as $r);
            redirect('file/update_file/'.$r["_id"]->__toString());
        }
        redirect('file');
    }

}