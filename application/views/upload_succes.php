<div class="content-wrapper">

<section class="content">

	
	<div class="row">
	     <div class="col-md-6">
	     	<h3>Your file was successfully uploaded!</h3>
			<ul>
			<?php foreach ($upload_data as $item => $value):?>
			<li><?php echo $item;?>: <?php echo $value;?></li>
			<?php endforeach; ?>
			</ul>
		</div>
	</div>

	

<p><?php echo anchor('Digitizer', 'Upload Another File!'); ?></p>
</section>
</div>