<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Get your XML File
      <!-- <small>and find out how awesome your file</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">XML</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Upload box -->
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Upload Here</h3>
          </div>
          <div class="box-body">
            <?php echo form_open_multipart('upload/do_upload');?>  
            <input type="file" name="userfile" size="20" style="visibility:hidden;" id="choosefile" />
            <div class="input-group">
              <input type="text" id="subfile" class="form-control" disabled="disabled" placeholder="Choose Your File">
              <span class="input-group-btn">
                <button type="button" class ="btn btn-success btn-flat bg-olive" onclick="$('#choosefile').click();"><i class="fa fa-search"></i> Browse  </button>
              </span>
            </div>
            <br/><br/>
              <a style="margin:498px"></a>
            <input type="submit" value="Upload" class="btn btn-success btn-flat bg-olive"/>
            </form>

          </div><!-- /.box-body -->
        </div>
      </div>
      </div>



  <form action="<?php echo site_url('xml'); ?>" method="get">
      <!--Filter Search-->
      <div class="row">
        <div class="col-md-5">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Filter Search</h3>
            </div>
            <div class="box-body">
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    <!--Input-->
                    <div class="input-group input-group">
                      <div class="form-group">
                       <label><h5>
                       </label>
                       <a style="margin:150px"></a>
                       <select class="form-control" name="qfield1" value="<?php echo $qfield1;?>">
                        <option value="" <?php if($this->input->get('qfield1')=="") echo 'selected'; ?> > <i> Search by </i> </option>
                        <option value="title" <?php if($this->input->get('qfield1')=="title") echo 'selected'; ?> >Title</option>
                        <option value="creator" <?php if($this->input->get('qfield1')=="creator") echo 'selected'; ?> >Creator</option>
                        <option value="description" <?php if($this->input->get('qfield1')=="description") echo 'selected'; ?> >Description</option>
                      </select></h5>
                    </div>
                    <input type="text" class="form-control" placeholder="keyword/phrases" name="q" value="<?php echo $this->input->get('q'); ?>" />
                  </div> <!--Input-->
  
                </div><!-- /.box-header -->
              </div><!-- /.box -->

              <h4 class="box-title"><center>Parameter Big Data</center></h4>
              <br>
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Variety</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  <div class="form-group">
                    <label>
                      Image
                    </label>
                    <a style="margin:30px"></a>
                    <select name="imgfield1" value="<?php echo $imgfield1;?>">
                      <option value="" <?php if($this->input->get('imgfield1')=="") echo 'selected'; ?> >  Select </option>
                      <option value="Image1" <?php if($this->input->get('imgfield1')=="Image1") echo 'selected'; ?> >.gif</option>
                      <option value="Image2" <?php if($this->input->get('imgfield1')=="Image2") echo 'selected'; ?> >.png</option>
                      <option value="Image3" <?php if($this->input->get('imgfield1')=="Image3") echo 'selected'; ?> >.jpg</option>
                      <option value="Image4" <?php if($this->input->get('imgfield1')=="Image4") echo 'selected'; ?> >.jpeg</option>
                      <option value="Image5" <?php if($this->input->get('imgfield1')=="Image5") echo 'selected'; ?> >.tiff</option>
                      <option value="Image6" <?php if($this->input->get('imgfield1')=="Image6") echo 'selected'; ?> >.bmp</option>
                    </select>
                  </div>
                  <div class="form-group">
                   <label>
                    Document
                  </label>
                  <a style="margin:16px"></a>
                  <select name="docfield1" value="<?php echo $docfield1;?>">
                    <option value="" <?php if($this->input->get('docfield1')=="") echo 'selected'; ?> >  Select </option>
                    <option value="Doc1" <?php if($this->input->get('docfield1')=="Doc1") echo 'selected'; ?> >.pdf</option>
                    <option value="Doc2" <?php if($this->input->get('docfield1')=="Doc2") echo 'selected'; ?> >.docx</option>
                    <option value="Doc3" <?php if($this->input->get('docfield1')=="Doc3") echo 'selected'; ?> >.doc</option>
                    <option value="Doc4" <?php if($this->input->get('docfield1')=="Doc4") echo 'selected'; ?> >.xls</option>
                    <option value="Doc5" <?php if($this->input->get('docfield1')=="Doc5") echo 'selected'; ?> >.xlsx</option>
                    <option value="Doc6" <?php if($this->input->get('docfield1')=="Doc6") echo 'selected'; ?> >.ppt</option>
                    <option value="Doc7" <?php if($this->input->get('docfield1')=="Doc7") echo 'selected'; ?> >.pptx</option>
                    <option value="Doc8" <?php if($this->input->get('docfield1')=="Doc8") echo 'selected'; ?> >.odt</option>
                  </select>
                </div>
                <div class="form-group">
                 <label>
                  Audio
                </label>
                <a style="margin:30px"></a>
                <select name="audfield1" value="<?php echo $audfield1;?>">
                  <option value="" <?php if($this->input->get('audfield1')=="") echo 'selected'; ?> >  Select </option>
                  <option value="Aud1" <?php if($this->input->get('audfield1')=="Aud1") echo 'selected'; ?> >.mpeg</option>
                  <option value="Aud2" <?php if($this->input->get('audfield1')=="Aud2") echo 'selected'; ?> >.mp3</option>
                  <option value="Aud3" <?php if($this->input->get('audfield1')=="Aud3") echo 'selected'; ?> >.m4a</option>
                  <option value="Aud4" <?php if($this->input->get('audfield1')=="Aud4") echo 'selected'; ?> >.wav</option>
                  <option value="Aud5" <?php if($this->input->get('audfield1')=="Aud5") echo 'selected'; ?> >.mp4</option>
                  <option value="Aud6" <?php if($this->input->get('audfield1')=="Aud6") echo 'selected'; ?> >.wma</option>
                  <option value="Aud7" <?php if($this->input->get('audfield1')=="Aud7") echo 'selected'; ?> >.wave</option>
                  <option value="Aud8" <?php if($this->input->get('audfield1')=="Aud8") echo 'selected'; ?> >.aac</option>
                  <option value="Aud9" <?php if($this->input->get('audfield1')=="Aud9") echo 'selected'; ?> >.dlna000</option>
                </select>
              </div>
              <div class="form-group">
               <label>
                Video
              </label>
              <a style="margin:30px"></a>
              <select name="vidfield1" value="<?php echo $vidfield1;?>">
                <option value="" <?php if($this->input->get('vidfield1')=="") echo 'selected'; ?> >  Select </option>
                <option value="Vid1" <?php if($this->input->get('vidfield1')=="Vid1") echo 'selected'; ?> >.mp4</option>
                <option value="Vid2" <?php if($this->input->get('vidfield1')=="Vid2") echo 'selected'; ?> >.3gpp</option>
                <option value="Vid3" <?php if($this->input->get('vidfield1')=="Vid3") echo 'selected'; ?> >.flv</option>
                <option value="Vid4" <?php if($this->input->get('vidfield1')=="Vid4") echo 'selected'; ?> >.wmv</option>
                <option value="Vid5" <?php if($this->input->get('vidfield1')=="Vid5") echo 'selected'; ?> >.mebm</option>
                <option value="Vid6" <?php if($this->input->get('vidfield1')=="Vid6") echo 'selected'; ?> >.matroska</option>
              </select>
            </div>

          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-default collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Volume</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
          </div><!-- /.box-header -->
          <div class="box-body" style="display: none;">
            <div class="form-group">
               <label>
                1
              </label>
              <a style="margin:30px"></a>
              <select name="volfield1" value="<?php echo $volfield1;?>">
                <option value="" <?php if($this->input->get('vidfield1')=="") echo 'selected'; ?> >  Select </option>
                <option value="Vid1" <?php if($this->input->get('vidfield1')=="Vid1") echo 'selected'; ?> >1000kb - 10000kb</option>
                <option value="Vid2" <?php if($this->input->get('vidfield1')=="Vid2") echo 'selected'; ?> >10001kb - 100000kb</option>
                <option value="Vid3" <?php if($this->input->get('vidfield1')=="Vid3") echo 'selected'; ?> > > 1000001kb</option>
              </select>
            </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-default collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Velocity</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
          </div><!-- /.box-header -->
          <div class="box-body" style="display: none;">
            The body of the box
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-default collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Veracity</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
          </div><!-- /.box-header -->
          <div class="box-body" style="display: none;">
            The body of the box
          </div><!-- /.box-body -->
        </div><!-- /.box -->

        <!--category-->
        <div class="box box-default collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Value</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            </div><!-- /.box-tools -->
          </div><!-- /.box-header -->
          <div class="box-body" style="display: none;">
            <div class="form-group">
             <label>
              Category
            </label>
            <a style="margin:30px"></a>
            <select name="catfield1" value="<?php echo $catfield1;?>">
              <option value="" <?php if($this->input->get('catfield1')=="") echo 'selected'; ?> >  Select </option>
              <option value="Cat1" <?php if($this->input->get('catfield1')=="Cat1") echo 'selected'; ?> >architecture</option>
              <option value="Cat2" <?php if($this->input->get('catfield1')=="Cat2") echo 'selected'; ?> >arts</option>
              <option value="Cat3" <?php if($this->input->get('catfield1')=="Cat3") echo 'selected'; ?> >biology</option>
              <option value="Cat4" <?php if($this->input->get('catfield1')=="Cat4") echo 'selected'; ?> >chemistry</option>
              <option value="Cat5" <?php if($this->input->get('catfield1')=="Cat5") echo 'selected'; ?> >communications</option>
              <option value="Cat6" <?php if($this->input->get('catfield1')=="Cat6") echo 'selected'; ?> >computerscience</option>
              <option value="Cat7" <?php if($this->input->get('catfield1')=="Cat7") echo 'selected'; ?> >economics</option>
              <option value="Cat8" <?php if($this->input->get('catfield1')=="Cat8") echo 'selected'; ?> >engineering</option>
              <option value="Cat9" <?php if($this->input->get('catfield1')=="Cat9") echo 'selected'; ?> >geography</option>
              <option value="Cat10" <?php if($this->input->get('catfield1')=="Cat10") echo 'selected'; ?> >history</option>
              <option value="Cat11" <?php if($this->input->get('catfield1')=="Cat11") echo 'selected'; ?> >law</option>
              <option value="Cat12" <?php if($this->input->get('catfield1')=="Cat12") echo 'selected'; ?> >mathematics</option>
              <option value="Cat13" <?php if($this->input->get('catfield1')=="Cat13") echo 'selected'; ?> >physics</option>
              <option value="Cat14" <?php if($this->input->get('catfield1')=="Cat14") echo 'selected'; ?> >politics</option>
              <option value="Cat15" <?php if($this->input->get('catfield1')=="Cat15") echo 'selected'; ?> >psychology</option>
            </select>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /.box category-->

      <div class="row">
        <div class="col-md-offset-9">
          <input type="submit" name="btncari" class="btn btn-success btn-flat bg-olive" value="Search"/> 
        </div>
      </div><!--row-->
    </div>
      </form>
  </div>
</div><!-- /.box -->

<div class="col-md-7">
  <div class="box box-default ">
    <div class="box-body">
      <div class="box box-body box-solid">
        <?php if (count($results)>0): ?>
          <?php $i=1;  ?>
          <?php foreach($results as $k => $v): 
          ?>
          <h3><a href="<?php echo site_url('xml/result_bigdata').'/'.$v['idmeta']; ?>">
            <?php echo highlight_phrase($v['title'], $q, '<span style="color:#990000">', '</span>'); ?></a></h3>
            <h5>Filename : <?php echo $v['filename'];?></h5>
            <h5>Size : <?php echo $v['volume'];?></h5>
            <h5>Velocity : <?php echo $v['velocity'];?></h5>
            <h5><i class="fa fa-tag"></i><b>Creator : </b><?php echo $v['creator'];?>
            <h5><i class="fa fa-tag"></i><b>Author : </b><?php echo $v['author'];?>
              <a style="margin:10px"></a>
              <i class="fa fa-tag"></i><b>Category : </b><?php echo $v['category'];?></a>
            </h5>
            <?php if ($this->session->userdata('role') == 'admin') : ?>
              <!--tombol-->
              <button class="btn btn-info btn-flat" data-widget="collapse"><i class="fa fa-plus"></i>Description</button>
              <div  class="box box-body box-solid" style="display: none;">
                <?php echo highlight_phrase($v['description'], $q, '<span style="color:#990000">', '</span>'); ?>
              </div><!-- /.box-body -->
              <br>
            </br>
            <div class="btn bg-maroon btn-flat">
            <a href="<?php echo site_url('xml/delete_file').'/'.$v['idmeta'] ?>" onclick="return confirm('Are you sure to delete this file?')"><i class="fa fa-times"></i><font color="white">delete</font></a>
            </div>
            <a style="margin:3px"></a>
            <div class="btn bg-orange btn-flat">
              <a href="<?php echo site_url('xml/update_file').'/'.$v['idmeta'] ?>"><i class="fa fa-pencil"></i><font color="white">edit</font></a>
            </div>
            <a style="margin:3px"></a>
            <div class="btn bg-purple btn-flat">
              <a href="<?php echo site_url('xml/result_bigdata').'/'.$v['idmeta']; ?>"><font color="white">detail</font></a>
            </div>
            <!--XML-->
            <a style="margin:130px"></a>
            <div class="btn-group">
              <button type="button" class="btn bg-olive btn-flat dropdown-toggle" aria-expanded="false"><i class="fa fa-file-code-o"></i>
                XML
              </button>
              <button type="button" class="btn bg-olive btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="<?php echo site_url('xml/callxml/'.$v['idmeta']); ?>" target="_blank">XML Metadata</a>
                </li>
                <li>
                  <a href="<?php echo site_url('xml/callxml_bigdata/'.$v['idmeta']); ?>" target="_blank">XML Big Data</a>
                </li>
              </ul>
            </div><!--XML-->
          <?php endif; ?>

          </div>
          <div class="box box-body box-default">
            <?php $i++ ?>
          <?php endforeach;?>
        </div>
      <?php else:?>
        <p> Not Found</p>
      <?php endif ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div>
</div><!-- /.box -->


    </section><!-- /.content -->

</div><!-- /.content-wrapper -->
