
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      All Files
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">All Files</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- File box -->
    <div class="box">
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <th>Creator</th>
              <th>Date Upload</th>
              <?php if ($this->session->userdata('role') == 'admin') : ?>
              <th>Action</th>
              <?php endif; ?>
            </tr>
          </thead>
          <?php if (count($results) > 0): ?>
          <?php $i=1; ?>
          <?php foreach($results as $result => $fields):?>
          <tr>
            <td><?php echo $i ?></td>
            <td>
                <a href="<?php echo site_url('file/detail').'/'.$fields['idmeta']; ?>">
                <?php echo $fields['name'];?></a>
            </td>
            <td><?php echo $fields['creator'];?></td>
            <td><?php echo $fields['time'];?></td>
            <?php if ($this->session->userdata('role') == 'admin') : ?>
            <td><center>
              <a href="<?php echo site_url('file/delete_file').'/'.$fields['idmeta'] ?>" onclick="return confirm('Are you sure to delete this file?')"> <button  class="btn btn-primary btn-sm"><i class="fa fa-times"></i></button></a>
              <a href="<?php echo site_url('file/update_file').'/'.$fields['idmeta'] ?>"> <button  class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a></center></td>
            <?php endif; ?>   
          </tr>
          <?php $i++ ?>
          <?php endforeach ?>
          <?php else:?>
          <p> Not Found</p>
          <?php endif ?>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

