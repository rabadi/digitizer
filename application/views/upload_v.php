<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Upload Your File
      <small>and find out how awesome your file</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Upload</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Upload box -->
    <div class="row">
      <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Upload Here</h3>
          </div>
          <div class="box-body">
            <?php echo form_open_multipart('upload/do_upload');?>  
              <input type="file" name="userfile" size="20" style="visibility:hidden;" id="choosefile" />
                <div class="input-group">
                <input type="text" id="subfile" class="form-control" disabled="enabled" placeholder="Choose Your File">
                  <span class="input-group-btn">
                    <button type="button" class ="btn btn-primary" onclick="$('#choosefile').click();">Browse</button>
                  </span>
                </div>
                <br/><br/>     
                <input type="submit" value="Upload" class="btn btn-primary"/>
          </div><!-- /.box-body -->
        </div>
    </div>

    <!-- Latest box -->
      <div class="col-md-6">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Your Last File</h3>
          </div>
          <div class="box-body">
            <table class="table table-striped">
              <tr>
                <th>Title</th>
                <th>Upload Date</th>
              </tr>

              <?php if (count($results) > 0): ?>
                <?php foreach($results as $result => $fields):?>
              <tr>
                <td>
                    <a href="<?php echo site_url('file/detail').'/'.$fields['id']; ?>">
                    <?php echo $fields['name'];?></a>
                </td>
                <td><?php echo date ('j F Y', $fields['uploaded_at']->sec);?></td>
              </tr>
              <?php endforeach ?>
              <?php endif ?>
            </table>
          </div><!-- /.box-body -->
        </div>
      </div>
    </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
  $('#choosefile').change(function()
  {
    $('#subfile').val($(this).val());
  });
</script>
