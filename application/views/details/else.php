<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Example of Bootstrap 3 Dynamic Tabs</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
    $("#myTab a").click(function(e){
      e.preventDefault();
      $(this).tab('show');
    });
});
</script>
</head>
<!-- Right side column. Contains the navbar and content of the page -->
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (count($detail) > 0){echo  $detail['title'];}?>
    </h1> 
    <?php echo  $detail['creator'];?>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Details box -->
    <div class="row">

      <!--Big Data box-->
       <div class="col-md-6">
          <div class="box box-primary">
              <div class="box-body">
                  <h3><b>Parameter Big Data<b><h3>
                    <h5><b>Variety<b><h5>
                    <div class="dl-horizontal"> 
                      <?php foreach (isset($detil['variety']) ? $detil['variety'] : array() as $details => $fields) : ?>             
                          <dt><?= ucwords($details); ?></dt>
                          <dd><?= ucwords($fields); ?><dd>
                      <?php endforeach;?>
                    </div>
                    <h5><b>Volume<b><h5>
                    <div class="dl-horizontal"> 
                      <?php foreach (isset($detil['volume']) ? $detil['volume'] : array() as $details => $fields) : ?>             
                          <dt><?= ucwords($details); ?></dt>
                          <dd><?= ucwords($fields); ?><dd>
                      <?php endforeach;?>
                    </div>
                    <h5><b>Veracity<b><h5>
                    <div class="dl-horizontal"> 
                      <?php foreach (isset($detil['veracity']) ? $detil['veracity'] : array() as $details => $fields) : ?>             
                          <dt><?= ucwords($details); ?></dt>
                          <dd><?= ucwords($fields); ?><dd>
                      <?php endforeach;?>
                    </div>
                    <h5><b>Velocity<b><h5>
                    <div class="dl-horizontal"> 
                      <?php foreach (isset($detil['velocity']) ? $detil['velocity'] : array() as $details => $fields) : ?>             
                          <dt><?= ucwords($details); ?></dt>
                          <dd><?= ucwords($fields); ?><dd>
                      <?php endforeach;?>
                    </div>
                    <h5><b>Value<b><h5>
                    <div class="dl-horizontal"> 
                      <?php foreach (isset($detil['value']) ? $detil['value'] : array() as $details => $fields) : ?>             
                          <dt><?= ucwords($details); ?></dt>
                          <dd><?= ucwords($fields); ?><dd>
                      <?php endforeach;?>
                    </div>
              </div>
          </div>
        </div>

      <!-- Meta box -->  
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-body">

          <h5><b>Identification</b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['identification']) ? $detail['identification'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?><dd>
            <?php endforeach;?>
            </div>

          <h5><b>File Info</b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['fileinfo']) ? $detail['fileinfo'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?><dd>
            <?php endforeach;?>
            </div>

          <?php  if (!empty($detail['filestatus'])) : ?>
          <h5><b><?= ucwords("File Status") ?></b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['filestatus']) ? $detail['filestatus'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?></dd>
            <?php endforeach;?>
            </div>
          <?php endif;?> 

           <h5><b>Statistics</b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['statistics']) ? $detail['statistics'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?><dd>
            <?php endforeach;?>
            </div>

          <h5><b>Metadata</b></h5>
            <div class="dl-horizontal">       
              <?php foreach (isset($detail['metadata']) ? $detail['metadata'] : array() as $details => $fields) : ?>
              <?php if(isset($details) && !empty($details) && is_array($fields)) : ?>
              <?php foreach (isset($fields) ? $fields : array() as $key => $value) : ?>             
                <dt><?= ucwords($key); ?></dt>
                <dd><?= ucwords($value); ?></dd>
              <?php endforeach;?>
              <?php endif; ?>
              <?php endforeach; ?>
            </div>
            <br>  
          </div>
        </div>        
      </div><!-- /.meta box-body -->


      <!-- View box -->     
      <div class="col-md-6">
        <div class="box">
          <!-- Comment box header -->                  
          <div class="box-header with-border">
            <h3 class="box-title"><b>Comment</b></h3>
          </div>
          <?php if(count($detail['comments'])){ ?> 
          <?php foreach($detail['comments'] as $k => $v):?>                 
          <!-- Comment box body -->
          <div class="box-body chat" id="chat-box">
            <!-- chat item -->
            <div class="item">
              <a href="<?php echo site_url('user/view_user').'/'.$this->session->userdata('user_id');?>" class="name">
                <small class="text-muted pull-right"><i class="fa fa-clock-o"></i><?php echo date ('j F Y g:i a', $v["date"]->sec);?></small>
                <b><?php echo $v["fullname"];?></b>
              </a><br/>
              <?php echo $v["des_comment"];?>
            </div><!-- /.item -->
          </div><!-- /.comment box body -->
          <?php endforeach; ?>
          <?php } ?>

          <?php if(!empty($details)): ?>
          <form action="<?php echo site_url('file/insert_comment/'.$detail['_id']); ?>" method="post">
            <!-- Comment box submit -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-2">
                  <label>Description</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <textarea type="text" name="des_comment" rows="3" class="form-control" placeholder="Enter ..."> </textarea>
                  </div>
                </div>
                <?php endif; ?>
                <div class="pull-right">
                  <input type="submit" value="Submit" name="btnComment" class="btn btn-primary"/>
                </div>
              </div>
            </div><!--/.comment box submit -->
          </form>
        </div> 
      </div> 
    </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->