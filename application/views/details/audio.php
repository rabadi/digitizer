<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Example of Bootstrap 3 Dynamic Tabs</title>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){ 
    $("#myTab a").click(function(e){
      e.preventDefault();
      $(this).tab('show');
    });
});
</script>
</head>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php if (count($detail) > 0){echo  $detail['title'];}?>
    </h1> 
    <?php echo  $detail['creator'];?>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-body">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#sectionA">Viewer</a></li>
            <li><a href="#sectionB">Parameter Big Data</a></li>
            <li><a href="#sectionC">Parameter Metadata</a></li>
        </ul>
        <div class="tab-content">
        <div id="sectionA" class="tab-pane fade in active">
          <div class="box box-solid">
            <div class="box-header" style="text-align:right">
            <b><?php echo $detail['category'];?></b>
            </div>
            <div class="box-body">
              <audio controls>
                <source src="<?php echo base_url();?>uploads/raw_data/<?php echo $detail['fileinfo']['filename']; ?>" type="audio/mpeg">
                <source src="<?php echo base_url();?>uploads/raw_data/<?php echo $detail['fileinfo']['filename']; ?>" type="audio/wav">
              Your browser does not support HTML5 video.
              </audio>
              <h5><b>Description</b></h5>
              <?php echo $detail['description'];?><br/><br/>              
            </div>
          </div>
        </div>
        <div id="sectionC" class="tab-pane fade">
            <div class="box box-primary">
              <div class="box-body">
                <h3><b>Metadata</b></h3>
                <h5><b>Identification</b></h5>
                  <div class="dl-horizontal">       
                    <?php foreach (isset($detail['identification']) ? $detail['identification'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                    <?php endforeach;?>
                  </div>
                <h5><b>File Info</b></h5>
                  <div class="dl-horizontal">       
                    <?php foreach (isset($detail['fileinfo']) ? $detail['fileinfo'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                    <?php endforeach;?>
                  </div>
                <?php  if (!empty($detail['filestatus'])) : ?>
                <h5><b><?= ucwords("File Status") ?></b></h5>
                  <div class="dl-horizontal">       
                    <?php foreach (isset($detail['filestatus']) ? $detail['filestatus'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                    <?php endforeach;?>
                  </div>
                <?php endif;?> 
              <h5><b>Statistics</b></h5>
                <div class="dl-horizontal">       
                  <?php foreach (isset($detail['statistics']) ? $detail['statistics'] : array() as $details => $fields) : ?>             
                    <dt><?= ucwords($details); ?></dt>
                    <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
              <h5><b>Metadata</b></h5>
                <div class="dl-horizontal">       
                  <?php foreach (isset($detail['metadata']) ? $detail['metadata'] : array() as $details => $fields) : ?>
                    <?php if(isset($details) && !empty($details) && is_array($fields)) : ?>
                      <?php foreach (isset($fields) ? $fields : array() as $key => $value) : ?>             
                        <dt><?= ucwords($key); ?></dt>
                        <dd><?= ucwords($value); ?></dd>
                      <?php endforeach;?>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
        </div>
        <div id="sectionB" class="tab-pane fade">
            <h3><b>Parameter Big Data<b></h3>
                <h4><b>Variety : <b></h4>
                <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['variety']) ? $detil['variety'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
                <h4><b>Volume : <b></h4>
                <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['volume']) ? $detil['volume'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
                <h4><b>Veracity : <b></h4>
                <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['veracity']) ? $detil['veracity'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
                <h4><b>Velocity : <b></h4>
                <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['velocity']) ? $detil['velocity'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
                <h4><b>Value : <b></h4>
                <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['value']) ? $detil['value'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
                 <h5><b>Time</b></h5>
                 <div class="dl-horizontal"> 
                  <?php foreach (isset($detil['time']) ? $detil['time'] : array() as $details => $fields) : ?>             
                      <dt><?= ucwords($details); ?></dt>
                      <dd><?= ucwords($fields); ?></dd>
                  <?php endforeach;?>
                </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
