 <!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta -->
    <?php $this->load->view('templates/meta'); ?>

</head>
<body>
   
    <!-- Header -->
    <?php $this->load->view('templates/header'); ?>

	<!-- Sidebar -->
    <?php $this->load->view('templates/sidebar'); ?>
    
    <!-- Page Content -->
    <?php $this->load->view($page); ?>

    <!-- Footer -->
	<?php $this->load->view('templates/footer'); ?>
    
</body>
</html>