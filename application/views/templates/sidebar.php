<!-- Left side column. contains the sidebar -->
<!-- The Right Sidebar -->
<aside class="control-sidebar control-sidebar-light">
  <!-- Content of the sidebar goes here -->
   <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" style="height:auto">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="http://localhost/preme/assets/img/avatar3.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo $this->session->userdata('fullname');?></p>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo site_url('datamart'); ?>"><i class="fa fa-gears"></i> Datamart</a></li>
            <!--<li><a href="<?php echo site_url('search'); ?>"><i class="fa fa-search"></i> Search</a></li>-->
            <!-- <li><a href="<?php echo site_url('dashboard'); ?>">Dashboard</a></li> -->
            <!-- <li><a href="<?php echo site_url('file/user_file'); ?>"><i class="fa fa-folder-open"></i> My Files</a></li> -->
            <!--<li><a href="<?php echo site_url('file'); ?>"><i class="fa fa-folder-o"></i> Data Collection</a></li>-->
            <?php if ($this->session->userdata('role') == 'admin') : ?> 
            <li><a href="<?php echo site_url('user'); ?>"><i class="fa fa-group"></i> List User</a></li>
            <?php endif ?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
</aside>
<!-- The sidebar's background -->
<!-- This div must placed right after the sidebar for it to work-->
<div class="control-sidebar-bg"></div>
     