<!-- Footer -->
<footer class="main-footer">
<div class="pull-right hidden-xs">
  <b>PA</b>/15743
</div>
<strong>Big Datamart.</strong>
</footer>

<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/plugins/slimScroll/jquery.slimScroll.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/plugins/fastclick/fastclick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/assets/js/app.min.js"></script>
<script src="<?php echo base_url();?>/assets/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>/assets/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<script type="text/javascript">
  //datatables
  $(function () {
    $("#example1").dataTable();
    $('#example2').dataTable({
      "bPaginate": true,
      "bLengthChange": true,
      "bFilter": false,
      "bSort": true,
      "bInfo": true,
      "bAutoWidth": false
    });
  });

  //iCheck for checkbox and radio inputs
  $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
  });
</script>