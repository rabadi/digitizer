</head>
  <body class="skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <!-- header logo: style can be found in header.less -->
      <header class="main-header">
        <a href="#" class="logo"><b>Digitizer</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown">
                <a href="<?php echo site_url('user/view_user').'/'.$this->session->userdata('user_id');?>"> Logged as <?php echo $this->session->userdata('username');?></a>
              </li>
              <li>
                <a href="<?php echo site_url('login/logout')?>"> Logout</a>
              </li>
            </ul>
          </div>
        </nav>
      </header>