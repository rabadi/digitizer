    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <title><?php echo $title ?></title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/skins/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/daterangepicker/daterangepicker-bs3.css.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/ionslider/ion.rangeSlider.skinNice.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/plugins/ionslider/ion.rangeSlider.css">
    <link href="<?php echo base_url();?>/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>/assets/plugins/iCheck/all.css" rel="stylesheet" type="text/css" />    


    <script src="<?php echo base_url();?>/assets/js/jquery-2.1.1.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</script>

