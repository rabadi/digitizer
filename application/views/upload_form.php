<script src="<?php echo site_url('assets/plugins/papaparse/papaparse.js') ?>"></script> 
<div class="content-wrapper">

 <section class="content-header">
    <h1>
      Upload Your File
      <small>and find out how awesome your file</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Upload</li>
    </ol>
  </section>

<section class="content">

	<div class="row">
	     <div class="col-md-6">
	        <div class="box">
	          <div class="box-header with-border">
	            <h3 class="box-title">Upload Here</h3>
	          </div>
	          <div class="box-body">
	            <?php echo form_open_multipart('Digitizer/do_upload');?>  
	              <input type="file" name="userfile" size="20" style="visibility:hidden;" id="choosefile" />
	                <div class="input-group">
	                <input type="text" id="subfile" class="form-control" disabled="enabled" placeholder="Choose Your File">
	                  <span class="input-group-btn">
	                    <button type="button" class ="btn btn-primary" onclick="$('#choosefile').click();">Browse</button>
	                  </span>
	                </div>
	                <br/><br/>     
	                <input type="submit" value="Upload" class="btn btn-primary"/>
	          </div><!-- /.box-body -->
	        </div>
	    </div>
	 </div>

	 <div class="box">
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>LAS file Name</th>
              <!-- <th>Date Upload</th> -->
              <th>Action</th>
            </tr>
          </thead>
          <?php if (count($results)): ?>
          <?php $i=1 ?>
          <?php foreach($results as $result => $fields):?>
                    <tr>
            <td><?php echo $i ?></td>
            <td>
                <a  href="<?php echo site_url('Digitizer/parsing_csv').'/'.$fields['filename']; ?>">
                <?php echo $fields['filename'];?></a>
            </td>
            <!-- <td><?php echo date ('j F Y g:i a', $fields['time']->sec);?></td> -->
            <td><center>
              <a href="<?php echo site_url('file/delete_userfile').'/'.$fields['id'] ?>" onclick="return confirm('Are you sure to delete this file?')"> <button  class="btn btn-primary btn-sm"><i class="fa fa-times"></i></button></a>
              <a href="<?php echo site_url('Digitizer/readcsv').'/'.$fields['filename']; ?>"> <button  class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a>
              <!-- <a id="filecsv" ><button class="btn btn-primary btn-sm">cek</Button></a> -->
            </td>
          	<!-- <a><input type=button value="Click here" onclick="document.location='file://c:/Program Files/ZetaWare/Zetalog/zetalog.exe'"></a> </center></td> -->
          </tr>
          <?php $i++ ?>
          <?php endforeach ?>
          <?php endif ?>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->

</section>
</div>

<script type="text/javascript">
$(function(){
  $('#filecsv').click(function(){
    Papa.parse(fileInput."#filecsv", {
  complete: function(results) {
    console.log(results);
  }
});
})
  
</script>