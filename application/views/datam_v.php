<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Rain Datamart
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Datamart</li>
    </ol>
  </section>
 

   <!-- Main content -->
  <section class="content">

    <!-- Upload box -->
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Upload Here</h3>
            </div>
            <div class="box-body">
              <?php echo form_open_multipart('upload/do_upload');?>  
                <input type="file" name="userfile" size="20" style="visibility:hidden;" id="choosefile" />
                  <div class="input-group">
                  <input type="text" id="subfile" class="form-control" disabled="enabled" placeholder="Choose Your File">
                    <span class="input-group-btn">
                      <button type="button" class ="btn btn-primary" onclick="$('#choosefile').click();">Browse</button>
                    </span>
                  </div>
                  <br/><br/>     
                  <input type="submit" value="Upload" class="btn btn-primary"/>
                  </form>
            </div><!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-3">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-files-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Inventory</span>
                <span class="info-box-number"><?php echo $datacount?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
            </div><!-- /.info-box-content -->
          </div>
      </div>
      <div class="col-md-6">
        <div class="box">
           <div class="box-header with-border" style="background:#DD4B39">
              <h3 class="box-title" style="color:white">Variety Statistics</h3>
            </div>
          <div class="row">
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <h5 class="description-header"><i class="fa fa-file-image-o"></i>Image</h5>
                <span class="description-text" style="text-align:center;float:left"><?php echo $imagecount?></span>
              </div><!-- /.description-block -->
            </div>
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <h5 class="description-header"><i class="fa fa-file-text"></i>Document</h5>
                <span class="description-text" style="text-align:center;float:left"><?php echo $dokumencount?></span>
              </div><!-- /.description-block -->
            </div>
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <h5 class="description-header"><i class="fa fa-file-audio-o"></i>Audio</h5>
                <span class="description-text" style="text-align:center;float:left"><?php echo $audiocount?></span>
              </div><!-- /.description-block -->
            </div>
            <div class="col-sm-3 col-xs-6">
              <div class="description-block border-right">
                <h5 class="description-header"><i class="fa fa-film"></i>Video</h5>
                <span class="description-text" style="text-align:center;float:left"><?php echo $videocount?></span>
              </div><!-- /.description-block -->
            </div>
          </div>
        </div>
      </div>
  
    </div> <!--row upload-->
    
  <div class="row">
    <form action="<?php echo site_url('datamart/index/0');?>" method="get">
      <!--Filter Search-->
      <div class="row">
        <div class="col-md-4" style="margin-left:30px;">
          <div class="box">
            <div class="box-header with-border" style="background:#DD4B39">
              <h3 class="box-title" style="color:white">Filter Search</h3>
            </div>
            <div class="box-body">
              <h4 class="box-title"><center>Enter your Keyword</center></h4>
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">
                    <!--Input-->
                    <div class="input-group input-group">
                      <div class="form-group">
                       <label><h5>
                     
                       <a style="margin:150px"></a>
                       <select class="form-control" name="qfield1" value="<?php echo $search['qfield1'];?>">
                        <option value="" <?php if($this->input->get('qfield1')=="") echo 'selected'; ?> > <i> Search by </i> </option>
                        <option value="tittle" <?php if($this->input->get('qfield1')=="tittle") echo 'selected'; ?> >Title</option>
                        <option value="creator" <?php if($this->input->get('qfield1')=="creator") echo 'selected'; ?> >Creator</option>
                        <option value="description" <?php if($this->input->get('qfield1')=="description") echo 'selected'; ?> >Description</option>
                      </select></h5>  </label>
                    </div>
                    <input type="text" class="form-control" placeholder="keyword/phrases" name="q" value="<?php echo $this->input->get('q'); ?>" />
                  </div> <!--Input-->
                </div><!-- /.box-header -->
              </div><!-- /.box -->

              <h4 class="box-title"><center>Parameter Big Data</center></h4>
              <br>
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Variety</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body" style="display: none;">
                  <div class="form-group">
                    <label>
                      Image
                    </label>
                    <a style="margin:30px"></a>
                    <select name="imgfield1" value="<?php echo $search['variety']['img'];?>"> 
                       <option value="Image7" <?php if($this->input->get('imgfield1')=="Image7") echo 'selected'; ?> >all</option>
                      <option value="" <?php if($this->input->get('imgfield1')=="") echo 'selected'; ?> >  Select </option>
                      <option value="Image1" <?php if($this->input->get('imgfield1')=="Image1") echo 'selected'; ?> >.gif</option>
                      <option value="Image2" <?php if($this->input->get('imgfield1')=="Image2") echo 'selected'; ?> >.png</option>
                      <option value="Image3" <?php if($this->input->get('imgfield1')=="Image3") echo 'selected'; ?> >.jpg</option>
                      <option value="Image4" <?php if($this->input->get('imgfield1')=="Image4") echo 'selected'; ?> >.jpeg</option>
                      <option value="Image5" <?php if($this->input->get('imgfield1')=="Image5") echo 'selected'; ?> >.tiff</option>
                      <option value="Image6" <?php if($this->input->get('imgfield1')=="Image6") echo 'selected'; ?> >.bmp</option>
                    </select>
                  </div>
                  <div class="form-group">
                   <label>
                    Document
                  </label>
                  <a style="margin:16px"></a>
                  <select name="docfield1" value="<?php echo $search['variety']['doc'];?>">
                    <option value="" <?php if($this->input->get('docfield1')=="") echo 'selected'; ?> >  Select </option>
                    <option value="Doc9" <?php if($this->input->get('docfield1')=="Doc9") echo 'selected'; ?> >all</option>
                    <option value="Doc1" <?php if($this->input->get('docfield1')=="Doc1") echo 'selected'; ?> >.pdf</option>
                    <option value="Doc2" <?php if($this->input->get('docfield1')=="Doc2") echo 'selected'; ?> >.docx</option>
                    <option value="Doc3" <?php if($this->input->get('docfield1')=="Doc3") echo 'selected'; ?> >.doc</option>
                    <option value="Doc4" <?php if($this->input->get('docfield1')=="Doc4") echo 'selected'; ?> >.xls</option>
                    <option value="Doc5" <?php if($this->input->get('docfield1')=="Doc5") echo 'selected'; ?> >.xlsx</option>
                    <option value="Doc6" <?php if($this->input->get('docfield1')=="Doc6") echo 'selected'; ?> >.ppt</option>
                    <option value="Doc7" <?php if($this->input->get('docfield1')=="Doc7") echo 'selected'; ?> >.pptx</option>
                    <option value="Doc8" <?php if($this->input->get('docfield1')=="Doc8") echo 'selected'; ?> >.odt</option>
                  </select>
                </div>
                <div class="form-group">
                 <label>
                  Audio
                </label>
                <a style="margin:30px"></a>
                <select name="audfield1" value="<?php echo $search['variety']['aud'];?>">
                  <option value="" <?php if($this->input->get('audfield1')=="") echo 'selected'; ?> >  Select </option>
                  <option value="Aud10" <?php if($this->input->get('audfield1')=="Aud10") echo 'selected'; ?> >all</option>
                  <option value="Aud1" <?php if($this->input->get('audfield1')=="Aud1") echo 'selected'; ?> >.mpeg</option>
                  <option value="Aud2" <?php if($this->input->get('audfield1')=="Aud2") echo 'selected'; ?> >.mp3</option>
                  <option value="Aud3" <?php if($this->input->get('audfield1')=="Aud3") echo 'selected'; ?> >.m4a</option>
                  <option value="Aud4" <?php if($this->input->get('audfield1')=="Aud4") echo 'selected'; ?> >.wav</option>
                  <option value="Aud5" <?php if($this->input->get('audfield1')=="Aud5") echo 'selected'; ?> >.mp4</option>
                  <option value="Aud6" <?php if($this->input->get('audfield1')=="Aud6") echo 'selected'; ?> >.wma</option>
                  <option value="Aud7" <?php if($this->input->get('audfield1')=="Aud7") echo 'selected'; ?> >.wave</option>
                  <option value="Aud8" <?php if($this->input->get('audfield1')=="Aud8") echo 'selected'; ?> >.aac</option>
                  <option value="Aud9" <?php if($this->input->get('audfield1')=="Aud9") echo 'selected'; ?> >.dlna000</option>
                </select>
              </div>
              <div class="form-group">
               <label>
                Video
              </label>
              <a style="margin:30px"></a>
              <select name="vidfield1" value="<?php echo $search['variety']['vid'];?>">
                <option value="" <?php if($this->input->get('vidfield1')=="") echo 'selected'; ?> >  Select </option>
                <option value="Vid7" <?php if($this->input->get('vidfield1')=="Vid7") echo 'selected'; ?> >all</option>
                <option value="Vid1" <?php if($this->input->get('vidfield1')=="Vid1") echo 'selected'; ?> >.mp4</option>
                <option value="Vid2" <?php if($this->input->get('vidfield1')=="Vid2") echo 'selected'; ?> >.3gpp</option>
                <option value="Vid3" <?php if($this->input->get('vidfield1')=="Vid3") echo 'selected'; ?> >.flv</option>
                <option value="Vid4" <?php if($this->input->get('vidfield1')=="Vid4") echo 'selected'; ?> >.wmv</option>
                <option value="Vid5" <?php if($this->input->get('vidfield1')=="Vid5") echo 'selected'; ?> >.mebm</option>
                <option value="Vid6" <?php if($this->input->get('vidfield1')=="Vid6") echo 'selected'; ?> >.matroska</option>
              </select>
            </div>

                  </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-default collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Volume</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->
                  <div class="box-body" style="display: none;margin-left:30px;">
                   <div class="form-group">
                    <label>
                      Range Size
                    </label>
                    <a style="margin:30px"></a>
                    <select name="size1" value="<?php echo $search['volume']['size'];?>">
                      <option value="" <?php if($this->input->get('size1')=="") echo 'selected'; ?> >  Select </option>
                      <option value="0,25000" <?php if($this->input->get('size1')=="Vol1") echo 'selected'; ?> >0 - 25000kb</option>
                      <option value="25001,50000" <?php if($this->input->get('size1')=="Vol2") echo 'selected'; ?> >25001kb - 50000kb</option>
                      <option value="50001,75000" <?php if($this->input->get('size1')=="Vol3") echo 'selected'; ?> >50001kb - 75000kb</option>
                      <option value="75001, 100000" <?php if($this->input->get('size1')=="Vol4") echo 'selected'; ?> >75001kb - 100000kb</option>
                      <option value="100001" <?php if($this->input->get('size1')=="Vol5") echo 'selected'; ?> >>100000</option>
                    </select>
        
                   </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-default collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Velocity</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                      <div class="form-group">
                        <label>
                          <input type="checkbox" name="fitstime" class="minimal" <?php echo (isset($search['velocity']['fitstime']) AND $search['velocity']['fitstime'] == true) ? 'checked="checked"' : null ?>/>
                            FitsExecutionTime
                        </label>
                     </div>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box box-default collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Veracity</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->
                  <div class="box-body" style="display: none;">
                    <label>
                      Author :
                    </label>
                    <select name="author" value="<?php echo $search['veracity']['author'];?>" <?php echo (isset($search['veracity']['author']) AND $search['veracity'] == true) ? 'selected="selected"' : null ?> >
                         <option value="" <?php if($this->input->get('author')=="") echo 'selected'; ?> >  Select </option>
                         <?php foreach ($creators as $creator) { ?>
                                <option> 
                                      <?php echo $creator;?>
                                </option>
                              <?php }?>
                     </select>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->

                <!--category-->
                <div class="box box-default collapsed-box">
                  <div class="box-header with-border">
                    <h3 class="box-title">Value</h3>
                    <div class="box-tools pull-right">
                      <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div><!-- /.box-tools -->
                  </div><!-- /.box-header -->
                  <div class="box-body" style="display: none;">
                    <div class="form-group">
                     <label>
                      Category
                    </label>
                    <a style="margin:30px"></a>
                    <select name="catfield1" value="<?php echo $search['value']['kategory'];?>">
                      <option value="" <?php if($this->input->get('catfield1')=="") echo 'selected'; ?> >  Select </option>
                      <option value="Cat1" <?php if($this->input->get('catfield1')=="Cat1") echo 'selected'; ?> >architecture</option>
                      <option value="Cat2" <?php if($this->input->get('catfield1')=="Cat2") echo 'selected'; ?> >arts</option>
                      <option value="Cat3" <?php if($this->input->get('catfield1')=="Cat3") echo 'selected'; ?> >biology</option>
                      <option value="Cat4" <?php if($this->input->get('catfield1')=="Cat4") echo 'selected'; ?> >chemistry</option>
                      <option value="Cat5" <?php if($this->input->get('catfield1')=="Cat5") echo 'selected'; ?> >communications</option>
                      <option value="Cat6" <?php if($this->input->get('catfield1')=="Cat6") echo 'selected'; ?> >computer science</option>
                      <option value="Cat7" <?php if($this->input->get('catfield1')=="Cat7") echo 'selected'; ?> >economics</option>
                      <option value="Cat8" <?php if($this->input->get('catfield1')=="Cat8") echo 'selected'; ?> >engineering</option>
                      <option value="Cat9" <?php if($this->input->get('catfield1')=="Cat9") echo 'selected'; ?> >geography</option>
                      <option value="Cat10" <?php if($this->input->get('catfield1')=="Cat10") echo 'selected'; ?> >history</option>
                      <option value="Cat11" <?php if($this->input->get('catfield1')=="Cat11") echo 'selected'; ?> >law</option>
                      <option value="Cat12" <?php if($this->input->get('catfield1')=="Cat12") echo 'selected'; ?> >mathematics</option>
                      <option value="Cat13" <?php if($this->input->get('catfield1')=="Cat13") echo 'selected'; ?> >physics</option>
                      <option value="Cat14" <?php if($this->input->get('catfield1')=="Cat14") echo 'selected'; ?> >politics</option>
                      <option value="Cat15" <?php if($this->input->get('catfield1')=="Cat15") echo 'selected'; ?> >psychology</option>
                    </select>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box category-->

              <div class="row">
                <div class="col-md-offset-9">
                  <input style="display:inline;margin-left:-50px" type="submit" name="btncari" class="btn btn-success btn-flat bg-olive" value="Search"/> 
                  <!-- <input type="button" name="btnreset" class="btn btn-success btn-flat bg-olive" value="Reset" id="btn-clear"/>  -->
                  <button style="display:inline" type="submit" id="btn-clear" class="btn btn-primary">Reset</button>
                </div>
              </div><!--row-->
          </div>
              </div>
            </div>
        </form>
        

        <div class="col-md-7">
          <div class="box">
            <div class="box-header with-border" style="background:#DD4B39">
              <h5 class="box-title" style="color:white;text-align:center">Data</h5>
            </div>
            <div class="box-body" style="margin-left:20px">
              <?php if (count($results)>0): ?>
                 <?php $i=1;  ?>
                <?php foreach ($results as $counter => $result) :?>
                    <h3><a href="<?php echo site_url('datamart/search_result').'/'.$result['idmeta']; ?>">
                      <?php echo highlight_phrase($result['title'], $q, '<span style="color:#990000">', '</span>'); ?></a></h3>
                          <div class="filedesc" style="margin-left:10px;">
                            <h5 style="display:inline"> time upload : </h5><p style="display:inline;"><?php echo date("j F Y",$result['upload_at']->sec)?></p></br>
                            <h5 style="display:inline"> Author [Value] : </h5><p style="display:inline"><?php echo $result['creator']?></p><br>
                            <h5 style="display:inline"> file Size [Volume]: </h5><p style="display:inline"><?php echo $result['volume']?> bytes</p><br>
                            <h5 style="display:inline"> format [Variety] : </h5><p style="display:inline"><?php echo $result['format']?></p>
                             <h5 style="display:inline;margin-left:30px"> fitsExecutiontime [Velocity] : </h5><p style="display:inline"><?php echo $result['velocity']?></p><br>
                             <h5 style="display:inline"> Well-formed [Veracity] : </h5><p style="display:inline"><?php echo $result['veracity']?></p>
                             <h5 style="display:inline;margin-left:60px"> Category : </h5><p style="display:inline;"><?php echo $result['category']?></p></a><br>
                            <h5 style="display:inline"> Data Lastmodified : </h5><p style="display:inline;"><?php echo $result['time']?></p>
                        </div>
                         </br>
                          <div class="box box-default collapsed-box" style="width:300px">
                            <div class="box-header with-border">
                              <h3 class="box-title">Description file</h3>
                              <div class="box-tools pull-right">
                                <i class="fa fa-align-left"></i><button class="btn btn-box-tool" data-widget="collapse"></button>
                             </div><!--/.box-tools -->
                          </div><!--/.box-header -->
                         <div  class="box-body" style="display:none;width:600px;">
                           <?php echo $result['description']?>
                           </div><!--/.box-body -->
                        </div><!--/.box -->
                          <a href="<?php echo site_url('datamart/search_result').'/'.$result['idmeta']; ?>"><button type="button" class="btn btn-info" href="">Detail</button></a>
                          <a href="<?php echo base_url('uploads/raw_data').'/'.$result['name'];?>" download><button type="button" class="btn btn-danger">Download</button></a>
                          <a style="float:right;margin-right:30px" href="<?php echo site_url('datamart/update_file').'/'.$result['idmeta'] ?>"><i class="fa fa-edit"></i></a>
                          <a style="float:right;margin-right:20px" href="<?php echo site_url('datamart/delete_file').'/'.$result['idmeta'] ?>" onclick="return confirm('Are you sure to delete this file?')"><i class="fa fa-trash"></i></a>
                          <hr/>
                 <!-- <div class="box box-body box-default"> -->
                    <?php $i++ ?>
                    <?php endforeach;?>
                <!-- </div> -->
                  <div id="pagination" style="float:right">
                  <navf                  <?php echo $str_links;?>
                </nav>
              </div>
                <?php else:?>
                <p> Not Found</p>
              <?php endif ?>

            </div><!--boxbody-->
          </div><!--box-->
        </div><!--col-->
      </div><!--row-->
</section>

<script>
$(document).ready(function(){
    // $("#box-widget").activateBox();
    $('#btn-clear').click(function(){
      window.location = '<?php echo site_url('datamart') ?>';
      return false;
    })
});
</script>

