<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/ionicons.min.css">
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Rain Datamart
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">B-Mart</li>
    </ol>
  </section>
 

   <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-3">
          <div class="info-box bg-yellow">
            <span class="info-box-icon" style="margin-left:1px"><i class="fa fa-files-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Inventory</span>
                <span class="info-box-number"><?php echo $datacount?></span>
              <div class="progress">
                <div class="progress-bar" style="width: 50%"></div>
              </div>
            </div><!-- /.info-box-content -->
          </div>
      </div>
    </div>
    <!--//Variety statistics-->
    <div class="box-default">
    <div class="row">
        <div class="col-md-3">
          <div class="info-box">
            <span class="info-box-icon bg-aqua" style="margin-left:1px"><i class="fa fa-file-image-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Image</span>
              <span class="info-box-number"><?php echo $imagecount?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red" style="margin-left:1px"><i class="fa fa-file-text-o"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Text Dokumen</span>
              <span class="info-box-number"><?php echo $dokumencount?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div><!-- /.col -->
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green" style="margin-left:1px"><i class="fa fa-volume-up"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Audio</span>
              <span class="info-box-number"><?php echo $audiocount?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow" style="margin-left:1px"><i class="fa fa-video-camera"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Video</span>
              <span class="info-box-number"><?php echo $videocount?></span>
            </div><!-- /.info-box-content -->
          </div><!-- /.info-box -->
        </div><!-- /.col -->
      </div>
    </div>

    <!-- Upload box -->
     <div class="row">
       <form action="<?php echo site_url('dashboard');?>" method="get">
      <div class="col-md-12">
     <div class="box box-primary">
          <div class="box-header">
            <h4><b>Searching by Keyword</b></h4>
          </div>
          <div class="box-body">   
               <div class="input-group input-group" style="margin-left:300px">
                  <div class="form-group">
                   <label>Enter your keyword here<h5>
                   <a style="margin:150px"></a>
                   <select class="form-control" name="qfield1" value="<?php echo $qfield1;?>">
                    <option value="" <?php if($this->input->get('qfield1')=="") echo 'selected'; ?> > <i> Search by </i> </option>
                    <option value="tittle" <?php if($this->input->get('qfield1')=="tittle") echo 'selected'; ?> >Title</option>
                    <option value="creator" <?php if($this->input->get('qfield1')=="creator") echo 'selected'; ?> >Creator</option>
                    <option value="description" <?php if($this->input->get('qfield1')=="description") echo 'selected'; ?> >Description</option>
                  </select></h5></label>
                </div>
                <input type="text" class="form-control" placeholder="keyword/phrases" name="q" value="<?php echo $this->input->get('q'); ?>" />
              </div> <!--Input-->
            <input type="submit" name="btncari" class="btn btn-primary" value="Search"/>        
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        </div>
      </form>
    </div>
  
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border" style="background:#DD4B39">
          <h5 class="box-title" style="color:white;text-align:center">Data</h5>
        </div>
        <div class="box-body" style="margin-left:20px">
          <?php if (count($results)>0): ?>
             <?php $i=1;  ?>
            <?php foreach ($results as $counter => $result) :?>
                <h3><a href="<?php echo site_url('dashboard/search_result').'/'.$result['idmeta']; ?>">
                  <?php echo highlight_phrase($result['title'], $q, '<span style="color:#990000">', '</span>'); ?></a></h3>
                    <div class="filedesc" style="margin-left:10px;">
                        <h5 style="display:inline"> Author [Value] : </h5><p style="display:inline"><?php echo $result['creator']?></p></br>
                        <h5 style="display:inline"> file Size [Volume]: </h5><p style="display:inline"><?php echo $result['volume']?>kb</p></br>
                        <h5 style="display:inline"> format [Variety] : </h5><p style="display:inline"><?php echo $result['format']?></p></br>
                         <h5 style="display:inline"> Well-formed [Veracity] : </h5><p style="display:inline"><?php echo $result['veracity']?></p>
                         <h5 style="display:inline;margin-left:30px"> Category : </h5><p style="display:inline;"><?php echo $result['category']?></p></a></br>
                        <h5 style="display:inline"> Data Lastmodified : </h5><p style="display:inline;"><?php echo $result['time']?></p>
                    </div>
                     </br>
                      <div class="box box-default collapsed-box" style="width:300px">
                        <div class="box-header with-border">
                          <h3 class="box-title">Description file</h3>
                          <div class="box-tools pull-right">
                            <i class="fa fa-align-left"></i><button class="btn btn-box-tool" data-widget="collapse"></button>
                          </div><!--/.box-tools -->
                        </div><!--/.box-header -->
                        <div  class="box-body" style="display:none;width:600px;">
                          <?php echo $result['description']?>
                        </div><!--/.box-body -->
                    </div><!--/.box -->
                      </hr>
                <?php $i++ ?>
                <?php endforeach;?>
                <div id="pagination" style="float:right">
                  <nav>
                  <?php echo $str_links;?>
                </nav>
              </div>
            <?php else:?>
            <p> Not Found</p>
          <?php endif ?>
        </div><!--boxbody-->
      </div><!--box-->
    </div><!--col--> 
    <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Category Collection</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <tbody><tr>
              <th style="width: 10px">No</th>
              <th>Category</th>
              <th>Progress</th>
              <th style="width: 40px">data</th>
            </tr>
            <tr>
              <td>1.</td>
              <td>computer Science</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
                </div>
              </td>
              <td><span class="badge bg-red"><?php echo $computerscience;?> data</span></td>
            </tr>
            <tr>
              <td>2.</td>
              <td>Arts</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-yellow" style="width: 70%"></div>
                </div>
              </td>
              <td><span class="badge bg-yellow"><?php echo $arts;?> data</span></td>
            </tr>
            <tr>
              <td>3.</td>
              <td>Architecture</td>
              <td>
                <div class="progress progress-xs progress-striped active">
                  <div class="progress-bar progress-bar-primary" style="width: 5%"></div>
                </div>
              </td>
              <td><span class="badge bg-light-blue"><?php echo $architecturecount?> data</span></td>
            </tr>
            <tr>
              <td>4.</td>
              <td>Biology</td>
              <td>
                <div class="progress progress-xs progress-striped active">
                  <div class="progress-bar progress-bar-success" style="width: 10%"></div>
                </div>
              </td>
              <td><span class="badge bg-green"><?php echo $biologycount?> data</span></td>
            </tr>
            <tr>
              <td>5.</td>
              <td>Chemistry</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-danger" style="width: 3%"></div>
                </div>
              </td>
              <td><span class="badge bg-red"><?php echo $kimiacount;?> data</span></td>
            </tr>
            <tr>
              <td>6.</td>
              <td>Communications</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-primary" style="width: 10%"></div>
                </div>
              </td>
              <td><span class="badge bg-blue"><?php echo $komcount;?> data</span></td>
            </tr>
            <tr>
              <td>7.</td>
              <td>Economy</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-yellow" style="width: 10%"></div>
                </div>
              </td>
              <td><span class="badge bg-yellow"><?php echo $ecocount;?> data</span></td>
            </tr>
            <tr>
            <tr>
              <td>8.</td>
              <td>Enginering</td>
              <td>
                <div class="progress progress-xs progress-striped active">
                  <div class="progress-bar progress-bar-success" style="width: 30%"></div>
                </div>
              </td>
              <td><span class="badge bg-green"><?php echo $enginecount?> data</span></td>
            </tr>
            <tr>
              <td>9.</td>
              <td>Geography</td>
              <td>
                <div class="progress progress-xs progress-striped active">
                  <div class="progress-bar progress-bar-danger" style="width: 3%"></div>
                </div>
              </td>
              <td><span class="badge bg-red"><?php echo $ecocount?> data</span></td>
            </tr>
            <tr>
              <td>10.</td>
              <td>History</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-yellow" style="width: 1%"></div>
                </div>
              </td>
              <td><span class="badge bg-yellow"><?php echo $historycount;?> data</span></td>
            </tr>
            <tr>
              <td>11.</td>
              <td>Law</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-success" style="width: 3%"></div>
                </div>
              </td>
              <td><span class="badge bg-green"><?php echo $lawcount;?> data</span></td>
            </tr>
             <tr>
              <td>12.</td>
              <td>Mathematics</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-primary" style="width: 5%"></div>
                </div>
              </td>
              <td><span class="badge bg-blue"><?php echo $mathcount;?> data</span></td>
            </tr>
             <tr>
              <td>13.</td>
              <td>Physics</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-danger" style="width: 6%"></div>
                </div>
              </td>
              <td><span class="badge bg-red"><?php echo $physicscount;?> data</span></td>
            </tr>
            <tr>
              <td>14.</td>
              <td>Politics</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-yellow" style="width: 1%"></div>
                </div>
              </td>
              <td><span class="badge bg-yellow"><?php echo $politicscount;?> data</span></td>
            </tr>
            <tr>
              <td>15.</td>
              <td>Psychology</td>
              <td>
                <div class="progress progress-xs">
                  <div class="progress-bar progress-bar-success" style="width: 0%"></div>
                </div>
              </td>
              <td><span class="badge bg-green"><?php echo $psychologycount;?> data</span></td>
            </tr>
          </tbody></table>
        </div><!-- /.box-body -->
      </div><!-- /.box category-->
      </div><!--/col category-->

    
 
        </div><!--col-->
      </div><!--row-->
    </div>
</section>

<script>
$(document).ready(function(){
    // $("#box-widget").activateBox();
    $('#btn-clear').click(function(){
      window.location = '<?php echo site_url('datamart') ?>';
      return false;
    })
});
</script>

