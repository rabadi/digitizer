<style>
 .content-wrapper
 {
  height: 542px !important;
 }
</style>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Profile
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Profile</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- Profile box -->  
    <div class="col-md-5 col-md-offset-3">
      <div class="box box-primary">
        <div class="box-body">
          <div class="row">
          <?php if(!empty($users)): ?>
          <form action="<?php echo site_url('user/update_user/'.$users[0]['_id']); ?>" method="post">
            <div class="col-md-3">
              <label>Full Name</label>
            </div>
            <div class="col-md-9">
              <div class="form-group">
                <input type="text" class="form-control" name="fullname" value=" <?php  echo $users[0]['fullname'];?> "/>
              </div>
            </div>
            <div class="col-md-3">
              <label>Email</label>
            </div>
            <div class="col-md-9">
              <div class="form-group">
                <input type="text" class="form-control" name="email" value=" <?php  echo $users[0]['email'];?> "/>
              </div>
            </div>
            <div class="col-md-3">
              <label>Username</label>
            </div>
            <div class="col-md-9">
              <div class="form-group">
                <input type="text" class="form-control" name="username" value=" <?php  echo $users[0]['username'];?> "/>
              </div>
            </div>
            <div class="col-md-3">
              <label>Password</label>
            </div>
            <div class="col-md-9">
              <div class="form-group">
                <input type="password" class="form-control" name="password" value=" <?php  echo $users[0]['password'];?> "/>
              </div>
            </div>
            <?php endif; ?>
            <div class="col-md-1 col-md-offset-9">
              <input type="submit" value="Submit" name="btnSimpan" class="btn btn-primary"/>
            </div>
          </form>
          </div>
        </div>       
      </div>
    </div><!-- /.profile-box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

