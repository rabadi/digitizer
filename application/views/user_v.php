
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      List User
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Create User box -->
    <div class="row">
      <div class="col-md-4">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Create User</h3>
          </div>
          <div class="box-body">
          <form action="<?php echo site_url('user/create_user'); ?>" method="post">
            <div class="form-group has-feedback">
              <input type="text" name="fullname" id="fullname" class="form-control" placeholder="Full name"/>
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="text" name="email" id="email" class="form-control" placeholder="Email"/>
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="username" name="username" id="username" class="form-control" placeholder="Username"/>
              <span class="glyphicon glyphicon-asterisk form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
              <div class="col-xs-4 col-xs-offset-8">
                <input type="submit" value="Submit" class="btn btn-primary btn-block btn-flat"/>
              </div><!-- /.col -->
            </div>
          </form>
          </div><!-- /.box-body -->
        </div>
    </div>


    <!-- List User box -->
    <div class="col-md-8">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List User</h3>
        </div>
        <div class="box-body">
          <?php echo isset($pesan_sukses) ? $pesan_sukses : ''; ?>
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>Full name</th>
                <th>Email</th>
                <th>Joined at</th>
                <th>Action</th>
              </tr>
            </thead>
            <?php if (count($users) > 0): ?>
            <?php $i=1 ?>
            <?php foreach($users as $user => $u):?>
            <tr>
              <td><?php echo $i ?></td>
              <td>
                  <a href="<?php echo site_url('user/view_user').'/'.$u['id']; ?>">
                  <?php echo $u['fullname'];?></a>
              </td>
              <td><?php echo $u['email'];?>
              <td><?php echo date ('j F Y', $u['joined_at']->sec);?></td>
              <td><center>
                <a href="<?php echo site_url('user/delete_user').'/'.$u['id'] ?>"onclick="return confirm('Are you sure to delete this user?')"> <button  class="btn btn-primary btn-sm" ><i class="fa fa-times"></i></button></a>
                <a href="<?php echo site_url('user/update_user').'/'.$u['id'] ?>"> <button  class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a></center></td>
            </tr>
            <?php $i++ ?>
            <?php endforeach ?>
            <?php else:?>
            <p> Not Found</p>
            <?php endif ?>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

