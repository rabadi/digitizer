<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data List
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">My Files</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- File box -->
    <div class="box">
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <th>Date Upload</th>
              <th>Action</th>
            </tr>
          </thead>
          <?php if (count($results)): ?>
          <?php $i=1 ?>
          <?php foreach($results as $result => $fields):?>
                    <tr>
            <td><?php echo $i ?></td>
            <td>
                <a href="<?php echo site_url('file/detail').'/'.$fields['id']; ?>">
                <?php echo $fields['name'];?></a>
            </td>
            <td><?php echo date ('j F Y g:i a', $fields['time']->sec);?></td>
            <td><center>
              <a href="<?php echo site_url('file/delete_userfile').'/'.$fields['id'] ?>" onclick="return confirm('Are you sure to delete this file?')"> <button  class="btn btn-primary btn-sm"><i class="fa fa-times"></i></button></a>
              <a href="<?php echo site_url('file/update_file').'/'.$fields['id'] ?>"> <button  class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button></a></center></td>
          </tr>
          <?php $i++ ?>
          <?php endforeach ?>
          <?php endif ?>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

