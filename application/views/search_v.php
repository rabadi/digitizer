<style type="text/css">
.form-group {
margin-bottom: 5px;
font-size: 14px;
}
.label{
  font-weight: 600;
}
</style>



<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Advanced Search
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Search</li>
    </ol>
  </section>
<form class="form-horizontal" action="<?php echo site_url('search'); ?>" method="get">
         
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- Filter box -->
      <div class="col-md-3">
        <div class="box box-solid box-primary">
          <div class="box-header">
            <h3 class="box-title">Filter Search</h3>
          </div>
          <div class="box-body">
            <h5><b> File Type </b></h5>
            <div class="row">
              <div class="col-md-offset-2">
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="document" class="minimal"/>
                    Document
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="video" class="minimal"/>
                    Video
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="image" class="minimal"/>
                    Image
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="audio" class="minimal"/>
                    Audio
                  </label>
                </div>
              </div>
            </div>
            <h5><b> File Status </b></h5>
            <div class="row">
              <div class="col-md-offset-2">
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="wellformed" class="minimal"/>
                    Wellformed
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="valid" class="minimal"/>
                    Valid
                  </label>
                </div>
              </div>
            </div>
            <h5><b> File Category </b></h5>
            <div class="row">
              <div class="col-md-offset-2">
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="architecture" class="minimal"/>
                    Architecture
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="arts" class="minimal"/>
                    Arts
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="biology" class="minimal"/>
                    Biology
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="chemistry" class="minimal"/>
                    Chemistry
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="communications" class="minimal"/>
                    Communications
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="computerscience" class="minimal"/>
                    Computer Science
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="economics" class="minimal"/>
                    Economics
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="engineering" class="minimal"/>
                    Engineering
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="forestry" class="minimal"/>
                    Forestry
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="geography" class="minimal"/>
                    Geography
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="history" class="minimal"/>
                    History
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="law" class="minimal"/>
                    Law
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="mathematics" class="minimal"/>
                    Mathematics
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="physics" class="minimal"/>
                    Physics
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="politics" class="minimal"/>
                    Politics
                  </label>
                </div>
                <div class="form-group">
                  <label>
                    <input type="checkbox" name="psychology" class="minimal"/>
                    Psychology
                  </label>
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>




      <!-- Search box -->
      <div class="col-md-9">
        <div class="box box-primary">
          <div class="box-header">
            <h4><b>Advanced Keyword/Phrases</b></h4>
          </div>
          <div class="box-body">   
                 <div class="form-group">
                <div class="col-md-5 col-md-offset-2">
                  <input type="text" class="form-control" placeholder="keyword/phrases" name="q"value="<?php echo $q;?>">
                </div>
                <div class="col-md-5">
                <label class="col-md-2" ><h5><b> in </b></h5></label>
                  <div class="col-md-10">
                    <select class="form-control" name="qfield1" value="<?php echo $qfield1;?>">
                      <option value="title" <?php if($this->input->get('qfield1')=="title") echo 'selected'; ?> >Title</option>
                      <option value="creator" <?php if($this->input->get('qfield1')=="creator") echo 'selected'; ?> >Creator</option>
                      <option value="description" <?php if($this->input->get('qfield1')=="description") echo 'selected'; ?> >Description</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-2">
                  <select class="form-control" name="operator" value="<?php echo set_value('operator');?>">
                    <option value="and" <?php if($this->input->get('operator')=="and") echo 'selected'; ?> >AND</option>
                    <option value="or" <?php if($this->input->get('operator')=="or") echo 'selected'; ?> >OR</option>
                    <option value="not" <?php if($this->input->get('operator')=="not") echo 'selected'; ?> >NOT</option>
                  </select>
                </div>
                <div class="col-md-5">
                  <input type="text" name="q2" class="form-control" placeholder="keyword/phrases" value="<?php echo $q2;?>">
                </div>
                <div class="col-md-5">  
                  <label class="col-md-2" ><h5><b> in </b></h5></label>
                  <div class="col-md-10">
                    <select class="form-control" name="qfield2" value="<?php echo set_value('qfield2');?>">
                      <option value="title" <?php if($this->input->get('qfield2')=="title") echo 'selected'; ?> >Title</option>
                      <option value="creator" <?php if($this->input->get('qfield2')=="creator") echo 'selected'; ?> >Creator</option>
                      <option value="description" <?php if($this->input->get('qfield1')=="description") echo 'selected'; ?> >Description</option>
                    </select>
                  </div>
                </div>
              </div>
            <input type="submit" name="btncari" class="btn btn-primary" value="Search"/>        
          </div><!-- /.box-body -->
        </div><!-- /.box -->


      <div class="box">
        <div class="box-body">
        <?php if ($file): ?>
          <table id="example2" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Creator</th>
                <th>Date Upload</th>
              </tr>
            </thead>

            <?php $i=1;  ?>
            <?php foreach($file as $k => $v): 
              ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td>
                      <a href="<?php echo site_url('search/result').'/'.$v['_id']; ?>">
                      <?php echo highlight_phrase($v['title'], $q, '<span style="color:#990000">', '</span>'); ?></a>
                  </td>
                  <td><?php echo $v['creator'];?></td>
                  <td><?php echo date ('j F Y', $v['uploaded_at']->sec);?></td>
                </tr>
                <?php $i++ ?>
              <?php 
            ?>
            
            <?php endforeach;?>
            <?php else:?>
            <p> Not Found</p>
            <?php endif ?>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
  </section><!-- /.content -->
  </form>
</div><!-- /.content-wrapper -->