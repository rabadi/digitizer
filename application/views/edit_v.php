<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <!-- <?php if (count($detail) > 0){echo  $detail['title'];}?> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Details box -->
    <div class="row">

      <!-- Update box -->     
      <div class="col-md-6">
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">File Description</h3>
          </div>
          <div class="box-body">
            <div class="row">

            <?php if(!empty($details)): ?>

            <form action="<?php echo site_url('datamart/update_file/'.$details[0]['_id']); ?>" method="post">
                <div class="col-md-2">
                  <label>Title</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <input type="text" class="form-control" name="title" value=" <?php echo $details[0]['title'];?> "/>
                  </div>
                </div>
                <div class="col-md-2">
                  <label>Creator</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <input type="text" class="form-control" name="creator" value="<?php echo $details[0]['creator']; ?>"/>
                  </div>
                </div>
                <div class="col-md-2">
                  <label>Category</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <select class="form-control" name="category" value="<?php echo $category;?>">
                      <option value="architecture" <?php if($this->input->get('category')=="architecture") echo 'selected'; ?> >Architecture</option>
                      <option value="arts" <?php if($this->input->get('category')=="arts") echo 'selected'; ?>>Arts</option>
                      <option value="biology" <?php if($this->input->get('category')=="biology") echo 'selected'; ?>>Biology</option>
                      <option value="chemistry" <?php if($this->input->get('category')=="chemistry") echo 'selected'; ?>>Chemistry</option>
                      <option value="communications" <?php if($this->input->get('category')=="communications") echo 'selected'; ?>>Communications</option>
                      <option value="computer science" <?php if($this->input->get('category')=="computer science") echo 'selected'; ?>>Computer Science</option>
                      <option value="economics" <?php if($this->input->get('category')=="economics") echo 'selected'; ?>>Economics</option>
                      <option value="engineering" <?php if($this->input->get('category')=="engineering") echo 'selected'; ?>>Engineering</option>
                      <option value="eorestry" <?php if($this->input->get('category')=="forestry") echo 'selected'; ?>>Forestry</option>
                      <option value="geography" <?php if($this->input->get('category')=="geography") echo 'selected'; ?>>Geography</option>
                      <option value="history" <?php if($this->input->get('category')=="history") echo 'selected'; ?>>History</option>
                      <option value="law" <?php if($this->input->get('category')=="law") echo 'selected'; ?>>Law</option>
                      <option value="mathematics" <?php if($this->input->get('category')=="mathematics") echo 'selected'; ?>>Mathematics</option>
                      <option value="physics" <?php if($this->input->get('category')=="physics") echo 'selected'; ?>>Physics</option>
                      <option value="politics" <?php if($this->input->get('category')=="politics") echo 'selected'; ?>>Politics</option>
                      <option value="psychology" <?php if($this->input->get('category')=="psychology") echo 'selected'; ?>>Psychology</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-2">
                  <label>Description</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <textarea  rows="3" name="description" class="form-control" > <?php echo $details[0]['description'];?> </textarea>
                  </div>
                </div>

               <!--  <div class="col-md-2">
                  <label>Keywords</label>
                </div>
                <div class="col-md-10">
                  <div class="form-group">
                    <input type="text" class="form-control" name="keywords" value="<?php echo implode(", ", $details[0]["keywords"]) ;?>"placeholder="Enter ..."/>
                  </div>
                </div> -->
                <?php endif; ?>
                <div class="col-md-1 col-md-offset-10">
                  <input type="submit" value="Submit" name="btnSimpan" class="btn btn-primary"/>
                </div>
              </form>
            </div>
          </div>      
        </div>
      </div> 

      <!-- Meta box -->  
       <div class="col-md-6">
        <div class="box box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">File Characteristic</h3>
          </div>

          <div class="box-body">

          <h5><b>Identification</b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['identification']) ? $detail['identification'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?><dd>
            <?php endforeach;?>
            </div>

          <h5><b>File Info</b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['fileinfo']) ? $detail['fileinfo'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?><dd>
            <?php endforeach;?>
            </div>

          <?php  if (!empty($detail['filestatus'])) : ?>
          <h5><b><?= ucwords("File Status") ?></b></h5>
            <div class="dl-horizontal">       
            <?php foreach (isset($detail['filestatus']) ? $detail['filestatus'] : array() as $details => $fields) : ?>             
              <dt><?= ucwords($details); ?></dt>
              <dd><?= ucwords($fields); ?></dd>
            <?php endforeach;?>
            </div>
          <?php endif;?> 

          <h5><b>Metadata</b></h5>
            <div class="dl-horizontal">       
              <?php foreach (isset($detail['metadata']) ? $detail['metadata'] : array() as $details => $fields) : ?>
              <?php if(isset($details) && !empty($details) && is_array($fields)) : ?>
              <?php foreach (isset($fields) ? $fields : array() as $key => $value) : ?>             
                <dt><?= ucwords($key); ?></dt>
                <dd><?= ucwords($value); ?></dd>
              <?php endforeach;?>
              <?php endif; ?>
              <?php endforeach; ?>
            </div>

            <br>  
          </div>
        </div>        
      </div><!-- /.box-body -->
    
    </div><!-- /.box -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


