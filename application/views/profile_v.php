<style>
 .content-wrapper
 {
  height: 542px !important;
 }
</style>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Profile
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Profile</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- PP box --> 
    <div class="row">    
      <div class="col-md-2 col-md-offset-2">
        <div class="box box-solid">
          <div class="box-body">
              <img src="<?php echo base_url();?>assets/img/avatar2.png" class="img-circle" width="100%" height="auto"; />
          </div>
        </div>
      </div><!-- /.pp-box --> 

      <!-- Profile box -->  
      <div class="col-md-5">
        <div class="box box-primary">
          <div class="box-body">
            <div class="dl-horizontal">
            <dt>Full Name </dt>
            <dd><?php echo $user['fullname'];?></dd>
            <dt>Email </dt>
            <dd><?php echo $user['email'];?></dd>
            <dt>Username </dt>
            <dd><?php echo $user['username'];?></dd>
            <dt>Joined at </dt>
            <dd><?php echo date ('j F Y g:i a', $user['joined_at']->sec);?></dd>
            </div>
          </div>       
        </div>
      </div><!-- /.profile-box -->
    </div>
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

