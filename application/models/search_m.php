<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_m extends CI_Model {
  
    private $collection = 'meta_collab';

    var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

    function count_all()
    {
        return $this->mongo_db
        ->count($this->collection);
    }

    function get_all_file()
    {
      $this->mongo_db->addIndex('meta_collab', array('title' => -1));
      return
      $this->mongo_db
      ->orderBy(array('uploaded_at'=>1))
      ->get($this->collection);
    }

    function get_all_bigdata($limit,$offset)
    {
      $this->mongo_db->addIndex('bigdata', array('title' => -1));
      return
      $this->mongo_db
      ->orderBy(array('upload_at'=>-1))
      ->limit($limit)
      ->offset($offset)
      ->get('bigdata');
    }

   

    function get_new_file()
    {
      $this->mongo_db->addIndex('meta_collab', array('title' => -1));
      return 
      $this->mongo_db
      ->orderBy(array('uploaded_at'=>-1))
      ->limit(5)
      ->get($this->collection);
    }

    function get_user_file()
    {
      return 
      $this->mongo_db
      ->where(['user_id' => $this->session->userdata('user_id')])
      ->orderBy(array('uploaded_at'=>-1))
      ->get($this->collection);
    }

    function get_creator()
    {
       $this->mongo_db->addIndex('bigdata', array('creator' => -1));
      return
      $this->mongo_db
      ->select(array('big_data.value.creator'))
      ->get('bigdata');
    }

    function get_file_by_id($id)
    {
      return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->get($this->collection);
    }


    function get_bigdata_by_id($id)
    {
      $db = new MongoClient("localhost");
       $n = $db->rain;
        $collection = $n->bigdata;
        $id_meta =array('idmeta' =>$id);
        $cursor = $collection->find($id_meta);
        $detil='';
         foreach ( $cursor as $key => $value)
         {
              $detil =  $value;
          }
        return $detil;
    }
 

    function retry(){
      $this->mongo_db->distinct('meta_collab', array('category' => -1));
       return
      $this->mongo_db
      // ->orderBy(array('uploaded_at'=>1))
      ->get($this->collection);
    }

    function delete_by_id($id)
    {
      return $this->mongo_db
      // not work
      // ->where(['_id' => ($id)])
      ->where(['_id' => new MongoId($id)])
      ->delete($this->collection);
    }

     function delete_bigdata_by_id($id)
    { 
       $id_meta =array('idmeta' =>$id);
      return $this->mongo_db
      ->where($id_meta)
      ->delete('bigdata');
    }

    function search()
    {
      $this->mongo_db->addIndex('meta_collab', array('title' => -1));
      $this->mongo_db->addIndex('meta_collab', array('creator' => -1));
      
      $q = $this->input->get('q');
      $q2 = $this->input->get('q2');
      $qfield1 = $this->input->get('qfield1');
      $qfield2 = $this->input->get('qfield2');
      $operator = $this->input->get('operator');

      if($q!="" && $q2!=""){
        if($operator=="and"){
          $this->mongo_db->whereLike($qfield1,$q, 'i');
          $this->mongo_db->whereLike($qfield2,$q2, 'i');

        }elseif($operator=="or"){
          $this->mongo_db->whereOrLike(array($qfield2=> $q2, $qfield1=> $q ), 'i');
          
        }elseif($operator=="not"){
           $this->mongo_db->whereLike($qfield1,$q, 'i');
           $this->mongo_db->whereNotLike($qfield2, array($q2), 'i' );
        }
      } elseif($q!="" && $q2==""){
           $this->mongo_db->whereLike($qfield1,$q);
       }

      return $this->mongo_db
      ->orderBy(array('uploaded_at'=>-1))
      ->get($this->collection); 
    }

    function search_bigdata($limit,$offset)
    {
 
      $q = $this->input->get('q');
      $qfield1 = "big_data.value.".$this->input->get('qfield1');
      if($q!=""){
           $this->mongo_db->whereLike($qfield1,$q,'i');
           // var_dump($qfield1.$q);exit(); 
      }

       // $this->mongo_db->where('big_data.variety.Mimetype',''); 

        //Variety

        $imgfield1 = "big_data.variety.Mimetype".$this->input->get('imgfield1');

        $inValues = array(); 

        switch($this->input->get('imgfield1')){
            case "Image1"  :     
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/gif')); 
                         break;
            case "Image2"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/png'));
                         break;
            case "Image3"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/jpg'));
                         break;
            case "Image4"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/jpeg'));
                         break;
            case "Image5"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/tiff'));
                         break;
            case "Image6"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'image/bmp'));
                         break;  
            case "Image7" : $this->mongo_db->where(array('big_data.variety.Mimetype' => array('$in'=>
        array('image/jpeg', 'image/png','application/octet-stream', 'image/gif','image/jpg','image/tiff', 'image/bmp','image/vnd.adobe.photoshop'))));           
                        break;
        }

        $docfield1 = "big_data.variety.Mimetype".$this->input->get('docfield1');

        switch($this->input->get('docfield1')){
            case "Doc1"  :    
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/pdf')); 
                         break;
            case "Doc2"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
                         break;
            case "Doc3"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/msword'));
                         break;
            case "Doc4"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.ms-excel'));
                         break;
            case "Doc5"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.openxmlformats-officedocument.presentationml.presentation'));
                         break;
            case "Doc6"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.ms-powerpoint'));
                         break;
            case "Doc7"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
                         break;
            case "Doc8"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'application/vnd.oasis.opendocument.text'));
                         break;
            case "Doc9"  :
                          $this->mongo_db->where(array('big_data.variety.Mimetype' => array('$in'=>array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'Application/vnd.oasis.opendocument.text','application/vnd.ms-excel',
                     'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text'))));                           
                      break;
        }

        $audfield1 = "big_data.variety.Mimetype".$this->input->get('audfield1');

 

        switch($this->input->get('audfield1')){
            case "Aud1"  :   
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/mpeg')); 
                         break;
            case "Aud2"  :    
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/mp3')); 
                         break;
            case "Aud3"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/x-m4a'));
                         break;
            case "Aud4"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/wav'));
                         break;
            case "Aud5"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/mp4'));
                         break;
            case "Aud6"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/x-ms-wma'));
                         break;
            case "Aud7"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/x-wave'));
                         break;
           case "Aud8"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/x-hx-aac-adts'));
                         break; 
           case "Aud9"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'audio/vnd.dlna.adts'));
                         break;  
          case "Aud10"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype' => array('$in'=>array
                          ('audio/mpeg', 'audio/wav','audio/mp3','audio/wav','audio/x-m4a','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts'))));
                         break;                          
        }

        switch($this->input->get('vidfield1')){
            case "Vid1"  :    
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/mp4')); 
                         break;
            case "Vid2"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/3gpp'));
                         break;
            case "Vid3"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/x-flv'));
                         break;
            case "Vid4"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/x-ms-wmv'));
                         break;
            case "Vid5"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/webm'));
                         break;
            case "Vid6"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype'=>'video/x-matroska'));
                         break;
            case "Vid7"  :  
                         $this->mongo_db->orWhere(array('big_data.variety.Mimetype' => array('$in'
        =>array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska'))));
                         break;
                                   
        }


        //volume
        if($this->input->get('size1')){
           list($low, $high) = explode(',', $this->input->get('size1'), 2);
           $start= (int)$low;
           $end=(int)$high;
          $this->mongo_db->where(array('big_data.volume.size'=>array('$gt'=>$start,'$lt'=>$end)));
        }

        //velocity
        if($this->input->get('fitstime')=='on'){
          $this->mongo_db->orderBy(array('big_data.velocity.fitsExecutionTime' =>1));
        }

        //veracity
        if($this->input->get('author')){
          $author1 = $this->input->get('author');
          $this->mongo_db->whereLike('big_data.value.creator',$author1);
           // var_dump($author1);exit;
        }

        //value
        switch($this->input->get('catfield1')){
            case "Cat1"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'architecture')); 
                         break;
            case "Cat2"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'arts')); 
                         break;
            case "Cat3"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'biology')); 
                         break;
            case "Cat4"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'chemistry')); 
                         break;
            case "Cat5"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'communications')); 
                         break;
            case "Cat6"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'computer science')); 
                         break;
            case "Cat7"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'economics')); 
                         break;
            case "Cat8"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'engineering')); 
                         break;
            case "Cat9"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'geography')); 
                         break;
            case "Cat10"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'history')); 
                         break;
            case "Cat11"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'law')); 
                         break;
            case "Cat12"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'mathematics')); 
                         break;
            case "Cat13"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'physics')); 
                         break;
            case "Cat14"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'politics')); 
                         break;
            case "Cat15"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'psychology')); 
                         break;
            case "Cat16"  :     
                         $this->mongo_db->orWhere(array('big_data.value.category'=>'arts')); 
                         break;        
            } 

        
      return $this->mongo_db
      ->orderBy(array('uploaded_at'=>-1))
      ->limit($limit)
      ->offset($offset)
      ->get('bigdata'); 
      }

    function insert_data($id,$data)
    {
      return $this->mongo_db
        ->where(['_id' => new MongoId($id)])
        ->push(['title' => $title,
                'creator' => $creator,
                'category' => $category,
                'description' => $description])
        ->update($this->collection);
    }

    function update_data($id,$data)
    {
    return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->set(['title' => $data['title'],
              'creator' => $data['creator'],
              'category' => $data['category'],
              'description' => $data['description']])
      ->update($this->collection);
    } 

      function update_bigdata($id,$data)
    { 
      $id_meta =array('idmeta' =>$id);
       return $this->mongo_db
      ->where($id_meta)
      ->set(['big_data.value.tittle' => $data['title'],
              'big_data.value.creator' => $data['creator'],
              'big_data.value.category' => $data['category'],
              'big_data.value.description' => $data['description']])
      ->update('bigdata');
    } 

    function insert_comment($id,$comment)
    {

      return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->push('comments', ['user_id' => $comment['user_id'],
                         'fullname'  => $comment['fullname'],
                         'des_comment' => $comment['des_comment'],
                         'date' => $comment['date']])
      ->update($this->collection);
    }

    function count_data()
    {
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;

      $tes = $collection->count();
      return $tes;
    }

    

    function count_image(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;

      $file_image = $collection->find(array('big_data.variety.Mimetype' => array('$in'=>
        array('image/jpeg', 'image/png','application/octet-stream', 'image/gif','image/jpg','image/tiff', 'image/bmp','image/vnd.adobe.photoshop'))));
      // var_dump($file_image);exit();
      $jml_image = $file_image->count();
      return $jml_image;
    }

    function count_dokumen(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;

      $file_doku = $collection->find(array('big_data.variety.Mimetype' => array('$in'
        =>array('application/pdf', 'application/vnd.oasis.opendocument.presentation','application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation','application/msword', 'Application/vnd.oasis.opendocument.text','application/vnd.ms-excel',
                     'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.oasis.opendocument.text'))));
     
      $jml_doku = $file_doku->count();
      return $jml_doku;
    }

    function count_audio(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;

      $file_audio = $collection->find(array('big_data.variety.Mimetype' => array('$in'
        =>array('audio/mpeg', 'audio/wav','audio/mp3','audio/wav','audio/x-m4a','audio/x-ms-wma','audio/x-wave','audio/x-hx-aac-adts', 'audio/mp4','audio/vnd.dlna.adts'))));
     
      $jml_audio = $file_audio->count();
      return $jml_audio;
    }

    function count_video(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;

      $file_video = $collection->find(array('big_data.variety.Mimetype' => array('$in'
        =>array('video/mp4','video/3gpp','video/mpeg','video/x-flv','video/x-ms-wmv', 'video/webm', 'video/x-matroska'))));
     
      $jml_video = $file_video->count();
      return $jml_video;
    }

    function keycount()
    {
      //define the map function
      $map = new MongoCode("function(){".
        "for (i=0; i < this.keywords.length; i++ {".
          "emit(this.keywords[i], 1);".
        "}".
      "}");
      //define the reduce function
      $reduce = new MongoCode("function(key, values) {".
        "var count=0;".
        "for (var i = 0; i < values.length; i++ {".
          "count += values[i];".
        "}".
        "return count;".
      "}");
      //run the map and reduce function, store results in a collection named keycount
      $command = array(
        'mapreduce' => 'meta_collab',
        'map' => $map,
        'reduce' => $reduce,
        'out' => 'keycount'
      );
      $key = $this->mongo_db->command($command);
      
      // load all the tags in an arry, sorted by frequent
      $keys = iterator_to_array($this->mongo_db
      ->orderBy(array('value' => -1))
      ->limit(10)
      ->get('tagcount'));

      return $key;
    }

  }