<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Count_m extends CI_Model {

	private $collection = 'meta_collab';

    var $field = '';
    var $query = '';

	 public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

      function search_bigdata($limit,$offset)
    {
 
      $q = $this->input->get('q');
      $qfield1 = "big_data.value.".$this->input->get('qfield1');
      if($q!=""){
           $this->mongo_db->whereLike($qfield1,$q,'i');
           // var_dump($qfield1.$q);exit(); 
      }
    }

  	 function count_cs(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_computer = $collection->find(array('big_data.value.category' => array('$in'=>array('computer science'))));
      $jml_comp = $file_computer->count();
      // var_dump($jml_comp);exit;
      return $jml_comp;
    }

    function count_architect(){

      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_arch = $collection->find(array('big_data.value.category' => array('$in'=>array('architecture'))));
      $jml_arch = $file_arch->count();
      return $jml_arch;
    }

     function count_art(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_art = $collection->find(array('big_data.value.category' => array('$in'=>array('arts'))));
      $jml_art = $file_art->count();
      return $jml_art;
    }

    function count_bio(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_bio = $collection->find(array('big_data.value.category' => array('$in'=>array('biology'))));
      $jml_bio = $file_bio->count();
      return $jml_bio;
    }

    function count_kimia(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_kimia = $collection->find(array('big_data.value.category' => array('$in'=>array('chemistry'))));
      $jml_kimia= $file_kimia->count();
      return $jml_kimia;
    }

     function count_komunikasi(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_komunikasi = $collection->find(array('big_data.value.category' => array('$in'=>array('communications'))));
      $jml_komunikasi= $file_komunikasi->count();
      return $jml_komunikasi;
    }

    function count_eco(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_economy = $collection->find(array('big_data.value.category' => array('$in'=>array('economics'))));
      $jml_economy= $file_economy->count();
      return $jml_economy;
    }

    function count_engineering(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_engineering = $collection->find(array('big_data.value.category' => array('$in'=>array('engineering'))));
      $jml_engineering= $file_engineering->count();
      return $jml_engineering;
    }

     function count_geo(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_geo = $collection->find(array('big_data.value.category' => array('$in'=>array('geography'))));
      $jml_geo= $file_geo->count();
      return $jml_geo;
    }

      function count_history(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_history = $collection->find(array('big_data.value.category' => array('$in'=>array('history'))));
      $jml_history= $file_history->count();
      return $jml_history;
    }

    function count_law(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_law = $collection->find(array('big_data.value.category' => array('$in'=>array('law'))));
      $jml_law= $file_law->count();
      return $jml_law;
    }

    function count_mathematics(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_mathematics = $collection->find(array('big_data.value.category' => array('$in'=>array('mathematics'))));
      $jml_mathematics= $file_mathematics->count();
      return $jml_mathematics;
    }

    function count_physics(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_physics = $collection->find(array('big_data.value.category' => array('$in'=>array('physics'))));
      $jml_physics = $file_physics->count();
      return $jml_physics;
    }

    function count_politics(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_politics = $collection->find(array('big_data.value.category' => array('$in'=>array('politics'))));
      $jml_politics = $file_politics->count();
      return $jml_politics;
    }

    function count_psychology(){
      $m = new MongoClient();
      $db = $m->rain;
      $collection = $db->bigdata;
      $file_psychology = $collection->find(array('big_data.value.category' => array('$in'=>array('psychology'))));
      $jml_psychology = $file_psychology->count();
      return $jml_psychology;
    }
}