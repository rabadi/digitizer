<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File_m extends CI_Model {
  
    private $collection = 'meta_collab';

    var $field = '';
    var $query = '';
    
    public function __construct()
  	{
  		  parent::__construct();
        $this->load->library('mongo_db');
  	}

    function count_all()
    {
        return $this->mongo_db
        ->count($this->collection);
    }

    function get_file()
    {
      $this->mongo_db->addIndex('lasfile', array('title' => -1));
      return
      $this->mongo_db
      ->orderBy(array('upload_at'=>-1))
      ->get('lasfile');
    }

     function get_file_by_id($filename)
    {
      return $this->mongo_db
      ->select(['upload_data.raw_name' => $filename])
      ->get($this->collection);
    }
}