<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_m extends CI_Model {
  
    private $collection = 'users';

    
    public function __construct()
  	{
  		  parent::__construct();
  	}

    // insert registration data in database
    function create_user($user)
    {
      return $this->mongo_db
      ->insert($this->collection, $user);
    }

    function cek_login($user, $pass)
    {
        $users = ['username' => $user,
                 'password' => $pass];

        return $this->mongo_db
        ->where($users)
        ->get($this->collection);
    }

    function count_all()
    {
        return $this->mongo_db
        ->count($this->collection);
    }

    function get_all_user()
    {

      return $this->mongo_db->get($this->collection);
    }

    function get_user_by_id($id)
    {

      return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->get($this->collection);
    }

    function delete_by_id($id)
    {
      return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->delete($this->collection);
    }

    function update_user($id,$u)
    {
      return $this->mongo_db
      ->where(['_id' => new MongoId($id)])
      ->set(['fullname' => $u['fullname'],
              'email' => $u['email'],
              'username' => $u['username'],
              'password' => $u['password']])
      ->update($this->collection);
    }    


}